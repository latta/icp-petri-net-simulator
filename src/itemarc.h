/**
 * @file itemarc.h
 * @author Martin Latta
 * @author Michal Hradecky
 */

#ifndef ITEMARC_H
#define ITEMARC_H

#include "itemnode.h"
#include "editorscene.h"

#include <QtCore>
#include <QtGui>

class ItemNode;
class EditorScene;

/**
  * Trida popisujici graficky objekt hrana (arc).
  * Algoritmus vykresleni hrany je inspirovan Diagram Scene Example na:
  * http://doc.qt.nokia.com/4.7-snapshot/graphicsview-diagramscene.html
  * @author Martin Latta
  * @author Michal Hradecky
  */
class ItemArc : public QGraphicsLineItem
{

public:
    ItemArc(ItemNode *f, ItemNode *t, QColor * color, EditorScene * s);
    ~ItemArc();

    enum { Type = UserType + 3};

    int type() const { return Type; }

    EditorScene * scene; /**< scena na kterou se arc vaze */

    QPointF Pos_from;   /**< pozice ze ktere hrana vychazi */
    QPointF Pos_to;     /**< pozice na ktere hrana konci */

    QString ID;         /**< identifikator ve tvaru: TxPx | PxTx, identifikuje odkud kam arc vede */
    QString arc_name;   /**< uzivatelske oznaceni (promenna nebo konstanta) */

    QGraphicsSimpleTextItem *id_item;   /**< textovy objekt - popisek u hrany na scene */

    qreal rand;         /**< nahodne cislo urcujici posunuti hrany */
    QColor arcColor;    /**< barva hrany */


    ItemNode *from;     /**< ukazatel na pocatecni uzel */
    ItemNode *to;       /**< ukazatel na koncovy uzel */


    QRectF boundingRect() const;
    void changeID(QString name);

protected:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

private:
    QColor *text_color;     /**< barva textu */

};

#endif // ITEMARC_H
