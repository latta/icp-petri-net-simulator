/**
 * @file logindialog.cpp
 * @author Martin Latta
 * @author Michal Hradecky
 */

#include "logindialog.h"
#include "ui_logindialog.h"

/**
  * Inicializuje prihlasovaci dialog
  */
LoginDialog::LoginDialog(QWidget *parent) :
    QDialog(parent),
    ui_login(new Ui::LoginDialog)
{
    ui_login->setupUi(this);
    ui_login->login->setFocus();

}

LoginDialog::~LoginDialog()
{
    delete ui_login;
}

/**
  * Nastavi do textoveho pole pozadovany status
  * @param status   novy status
  */
void LoginDialog::setStatus(QString status)
{
    ui_login->label_status->setText(status);
}

/**
  * Vraci text ktery uzivatel zadal do textoveho pole "login"
  * @return uzivatelem zadane jmeno
  */
QString LoginDialog::getLogin()
{
    return ui_login->login->text();
}

/**
  * Vraci text ktery uzivatel zadal do textoveho pole "password"
  * @return uzivatelem zadane heslo
  */
QString LoginDialog::getPassword()
{
    return ui_login->password->text();
}

/**
  * Smaze aktualni obsah textovych poli v dialogu
  */
void LoginDialog::clearData()
{
    ui_login->login->clear();
    ui_login->password->clear();
}

/**
  * Emituje signal kdyz uzivatel klikne na tlacitko "Storno"
  */
void LoginDialog::on_Btn_storno_clicked()
{
    emit click_storno();
    LoginDialog::done(0);

}

/**
  * Emituje signal kdyz uzivatel klikne na tlacitko "OK"
  */
void LoginDialog::on_Btn_ok_clicked()
{
    emit click_login();
}

/**
  * Emituje signal kdyz uzivatel klikne na tlacitko "New user"
  */
void LoginDialog::on_Btn_new_user_clicked()
{
    emit click_newuser();
}
