/**
 * @file mainwindow.h
 * @author Martin Latta
 * @author Michal Hradecky
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "connectdialog.h"
#include "logindialog.h"
#include "editorscene.h"
#include "itemnode.h"

#include <QMainWindow>
#include <QObject>
#include <QAbstractSocket>
#include <QLabel>
#include <QtGui>
#include <QtCore>

class QTcpSocket;

namespace Ui {
class MainWindow;
}

/**
  * Trida reprezentujici hlavni dialog
  * @author Martin Latta
  * @author Michal Hradecky
  */
class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    QString host_name;      /**< nazev serveru */
    QString user_name;      /**< jmeno uzivatele */
    int port;               /**< port pro pripojeni */
    QTcpSocket * socket;    /**< komunikacni docket */

    ConnectDialog *connect_form;    /**< dialog pro pripojeni na server */
    LoginDialog *login_form;        /**< dialog pro prihlaseni na server */


    QLabel * server_status_label;   /**< text ve status baru */
    QLabel * user_status_label;     /**< text ve status baru */

    QList<QStringList> *config_list;    /**< seznam s jednotlivymi styly */
    int * current_style;                /**< cislo aktualniho stylu */
    QColor * p_color;           /**< aktualni barva mist */
    QColor * t_color;           /**< aktualni barva prechodu */
    QColor * txt_color;         /**< aktualni barva textu u hran */


signals:
    void windowcreated();

private slots:
    void openConnectDialog();
    void openLoginDialog();

    void doConnect();
    void doDisconnect();
    void doLogin();
    void doNewUser();
    void openNetLocal();
    void saveNet();
    void saveNetAs();
    void openNetFromServer();
    void saveNetOnServer();
    void aboutDialog();

    void toolGroupClicked();

    void addNewTabSlot();
    void tabSwitched();
    void deleteAll();
    void deleteItem();
    void closeCurrentTab();
    void closeTab(int index);
    void editProperties();
    void showHistory();

    void stepDialogChange(QTreeWidgetItem * item, int);

    void simulationRun();
    void simulationStep();

    void changeStyle(int style);

private:
    void setupToolbar();
    void addNewTab(QString tabtitle);
    void closeEvent(QCloseEvent *event);
    void keyPressEvent(QKeyEvent *);
    void readSettings();
    void writeSettings();


    QPushButton * simulationRunButton;  /**< tlacitko simulace */
    QPushButton * simulationStepButton; /**< tlacitko kroku simulace */
    QPushButton * historyButton;        /**< tlacitko historie simulaci */
    QTabWidget * tabs;                  /**< taby */
    QToolBar * editToolbar;             /**< toolbar */
    QButtonGroup * editToolGroup;       /**< skupiny tlacitek v toolbaru */
    QButtonGroup * editToolGroup2;
    QDialogButtonBox *bbox;

    Ui::MainWindow *ui;

    int tempCount;
};

#endif // MAINWINDOW_H
