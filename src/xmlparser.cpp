/**
 * @file xmlparser.cpp
 * @author Martin Latta
 * @author Michal Hradecky
 */

#include "xmlparser.h"
#include <QMessageBox>

/**
 * Ulozeni ukazatelu z rodice
 * @param editorScene ukazatel na scenu do ktere se nactou informace z Xml
 */
XmlParser::XmlParser(EditorScene * editorScene)
{
    scene = editorScene;
}

/**
 * Ulozeni ukazatelu z rodice
 * @param conf_list ukazatel na seznam konfiguraci, kam se nactou vsechny styly z Xml
 * @param curr_style ukazatel na styl na ktery bude defaultni styl x Xml aplikovan
 */
XmlParser::XmlParser(QList<QStringList> * conf_list,int *curr_style)
{
    config_list = conf_list;
    current_style= curr_style;
}

/**
 * Otevre a zpracuje Xml soubor
 * @param finaName jmeno souboru ktery se ma zpracovat
 * @return vraci true, pokud se vse nacetlo v poradku, jinak vraci false
 */
bool XmlParser::readFile(const QString &fileName)
{
    QFile file(fileName);
    if (!file.open(QFile::ReadOnly | QFile::Text)) {
        QMessageBox::critical(this, tr("Error"),
                                 tr("Can't open file ")+fileName);
        return false;
    }
    reader.setDevice(&file);

    reader.readNext();
    while (!reader.atEnd())
    {
        if (reader.isStartElement())
        {
            if (reader.name() == "petri")
            {
                readPetriElement();
            }
            else
            {
                reader.raiseError(QObject::tr(" is not a valid petri net file"));
            }
        }
        else
        {
            reader.readNext();
        }
    }

    file.close();
    if (reader.hasError())
    {
        QMessageBox::critical(this, tr("Error in parsing file"),
                                 fileName+" "+reader.errorString() );
        return false;
    } else if (file.error() != QFile::NoError) {
        QMessageBox::critical(this, tr("Error in reading file"),
                                 fileName+" "+file.errorString() );
        return false;
    }
    return true;
}

/**
 * Zpracovani jednoho elementu z nacteneho Xml souboru
 */
void XmlParser::readPetriElement()
{
    reader.readNext();
    while (!reader.atEnd()) {
        if (reader.isEndElement()) {
            reader.readNext();
            break;
        }

        if (reader.isStartElement()) {
            if (reader.name() == "net") {
                readNetElement();
            }
            else if (reader.name() == "users") {
                readUsersElement();
            }
            else if (reader.name() == "config") {
                readConfigElement();
            }
            else
            {
                reader.raiseError(QObject::tr(" invalid petri net file. unknown tag ")+reader.name().toString());
                reader.readNext();
            }
        } else {
            reader.readNext();
        }
    }

}


/**
 * Zpracovani jednoho elementu z nacteneho Xml souboru
 */
void XmlParser::readNetElement()
{
    // nastaveni zakladnich atributu sceny
    scene->name = reader.attributes().value("name").toString();
    if (scene->name=="") scene->name="unnamed";
    scene->login = reader.attributes().value("login").toString();

    QString vers = reader.attributes().value("version").toString();
    if (vers=="") scene->version = 0;
    else scene->version = vers.toInt();

    scene->description = reader.attributes().value("description").toString();

    // tagy run, place, arc, transition jsou ve tvaru <run x="y" /> , maji za sebou imaginarni zaviraci tag
    reader.readNext();
    while (!reader.atEnd()) {
        if (reader.isEndElement()) {
            reader.readNext();
            break;
        }

        if (reader.isStartElement() ) {
            if (reader.name() == "transition") {
                readTransitionElement();
            } else if (reader.name() == "run") {
                readRunElement();
            } else if (reader.name() == "arc") {
                readArcElement();
            } else if (reader.name() == "place") {
                readPlaceElement();
            } else {
                reader.raiseError(QObject::tr(" invalid petri net file. unknown tag ")+reader.name().toString());
                reader.readNext();
            }
        } else {
            reader.readNext();
        }
    }

}

/**
 * Zpracovani jednoho elementu z nacteneho Xml souboru
 */
void XmlParser::readArcElement()
{
    scene->addArc( reader.attributes().value("start").toString(),
                   reader.attributes().value("stop").toString(),
                   reader.attributes().value("id").toString(),
                   reader.attributes().value("name").toString()
                );

    // musi byt dvakrat kvuli imaginarnimu uzaviracimu tagu
    reader.readNext();
    reader.readNext();
}

/**
 * Zpracovani jednoho elementu z nacteneho Xml souboru
 */
void XmlParser::readPlaceElement()
{    
    scene->addPlace(QPointF(reader.attributes().value("x").toString().toDouble(),reader.attributes().value("y").toString().toDouble()),
                    reader.attributes().value("id").toString(),
                    reader.attributes().value("tokens").toString()
                    );

    // musi byt dvakrat kvuli imaginarnimu uzaviracimu tagu
    reader.readNext();
    reader.readNext();
}

/**
 * Zpracovani jednoho elementu z nacteneho Xml souboru
 */
void XmlParser::readTransitionElement()
{
    scene->addTransition(QPointF(reader.attributes().value("x").toString().toDouble(),reader.attributes().value("y").toString().toDouble()),
                    reader.attributes().value("id").toString(),
                    reader.attributes().value("guards").toString(),
                    reader.attributes().value("expression").toString()
                    );

    // musi byt dvakrat kvuli imaginarnimu uzaviracimu tagu
    reader.readNext();
    reader.readNext();
}

/**
 * Zpracovani jednoho elementu z nacteneho Xml souboru
 */
void XmlParser::readRunElement()
{
    // musi byt dvakrat kvuli imaginarnimu uzaviracimu tagu
    reader.readNext();
    reader.readNext();
}

/**
 * Zpracovani jednoho elementu z nacteneho Xml souboru
 */
void XmlParser::readUsersElement()
{
    reader.raiseError(QObject::tr(" is not a valid petri net file. It has <users> tags."));
    reader.readNext();
}

/**
 * Zpracovani jednoho elementu z nacteneho Xml souboru
 */
void XmlParser::readConfigElement()
{
    // nacteni toho, ktery ze stylu bude aplikovany
    QString default_id = reader.attributes().value("default").toString();

    // nacteni vsech <style>, v kazdem je 1 styl
    reader.readNext();
    while (!reader.atEnd()) {
        if (reader.isEndElement()) {
            reader.readNext();
            break;
        }

        if (reader.isStartElement() ) {
            if (reader.name() == "style") {
                readStyleElement(default_id);
            } else {
                reader.raiseError(QObject::tr(" invalid config file. unknown tag ")+reader.name().toString());
                reader.readNext();
            }
        } else {
            reader.readNext();
        }
    }
}

/**
 * Zpracovani jednoho elementu z nacteneho Xml souboru
 */
void XmlParser::readStyleElement(QString default_id)
{
    //nacteni id
    int id = reader.attributes().value("id").toString().toInt();

    QStringList slist;

    if (default_id.toInt() == id)
        *current_style=id;

    // nacteni name
    QString name = reader.attributes().value("name").toString();
    if (name=="")
        slist << "Unknown";
    else
        slist << name;

    // nastaveni barvy mist
    QString pColor = reader.attributes().value("pcolor").toString();
    if (pColor=="")
            slist << "red";
        else
            slist << pColor;

    // nastaveni barvy prechodu
    QString tColor = reader.attributes().value("tcolor").toString();
    if (tColor=="")
        slist << "yellow";
    else
        slist << tColor;


    // nastaveni barvy textu
    QString textColor = reader.attributes().value("textcolor").toString();
    if (textColor=="")
        slist << "gray";
    else
        slist << textColor;

    // nastaveni barvy pozadi
    QString bColor = reader.attributes().value("backgroud").toString();
    if (bColor=="")
        slist << "white";
    else
        slist << bColor;

    // pridani do seznamu
    config_list->append(slist);


    // musi byt dvakrat kvuli imaginarnimu uzaviracimu tagu
    reader.readNext();
    reader.readNext();
}
