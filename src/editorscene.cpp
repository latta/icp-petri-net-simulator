/**
 * @file editorscene.cpp
 * @author Martin Latta
 * @author Michal Hradecky
 */

#include "editorscene.h"
#include "itemarc.h"
#include "itemnode.h"

#include <limits>
#include <QtGui>
#include <QtCore>

/**
 * Nastaveni vsech promennych na vychozi hodnoty
 * @param tabtitle  nazev sceny
 * @param p_color   barva mist
 * @param t_color   barva prechodu
 * @param txt_color   barva textu u hran
 */
EditorScene::EditorScene(QString tabtitle,QColor *p_color, QColor *t_color, QColor *txt_color)
{
    // nastaveni velikosti sceny
    setSceneRect(QRectF(-1000, -1000, 2000, 2000));

    action = ActMove;
    ID_P=0;
    ID_T=0;
    ID_P_max=0;
    ID_T_max=0;

    ID_A_char='a';
    saved=true;

    sceneArcList = new QHash<QString, ItemArc *>;
    scenePlaceList = new  QHash<QString, ItemNode *>;
    sceneTransitionList = new QHash<QString, ItemNode *>;


    place_color=p_color;
    trans_color=t_color;
    text_color=txt_color;

    login="";
    name=tabtitle;
    description="";
    version=1;
    fileName="";
}

/**
 * Smazou se vsechny seznamy grafickych objektu na scene
 */
EditorScene::~EditorScene()
{
    delete sceneArcList;
    delete scenePlaceList;
    delete sceneTransitionList;
}

/**
 * Reimplementovana metoda QGraphicsScene::mousePressEvent
 * Podle aktualne vybrane operace (action) se vykonaji dane akce pri kliknuti na scenu
 */
void EditorScene::mousePressEvent(QGraphicsSceneMouseEvent *event)
{

    QList<QGraphicsItem *> itemsonscene = items(event->scenePos());

    if (event->button() == Qt::LeftButton)
    {
        switch (action) {
        case ActMove:

            break;
        case ActInsPlace:
            if (itemsonscene.empty())
                // pridani mista na pozici kliknuti
                addPlace(event->scenePos(),QString(),QString());
            break;
        case ActInsTrans:
            if (itemsonscene.empty())
                // pridani prechodu na pozici kliknuti
                addTransition(event->scenePos(),QString(),QString(),QString());
            break;
        case ActInsArc:
            line = new QGraphicsLineItem(QLineF(event->scenePos(),event->scenePos()));
            addItem(line);
            break;

        }
    }

    QGraphicsScene::mousePressEvent(event);

}

/**
 * Reimplementovana metoda QGraphicsScene::mouseMoveEvent
 * Podle aktualne vybrane operace (action) se vykonaji dane akce pri pohybu mysi
 */
void EditorScene::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    if (action==ActMove)
    {
        update();
        QGraphicsScene::mouseMoveEvent(event);
    }
    else if (action==ActInsArc && line != 0)
    {
        QLineF newline(line->line().p1(),event->scenePos());
        line->setLine(newline);
    }
}

/**
 * Reimplementovana metoda QGraphicsScene::mouseReleaseEvent
 * Podle aktualne vybrane operace (action) se vykonaji dane akce pri uvolneni tlacitka mysi
 */
void EditorScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{


    if (action==ActInsArc && line != 0)
    {
        QList<QGraphicsItem *> startItems = items(line->line().p1());
        if (startItems.first() == line)
            startItems.removeFirst();
        QList<QGraphicsItem *> stopItems = items(line->line().p2());
        if (stopItems.first() == line)
            stopItems.removeFirst();

        // kontrola jestli neprekazi nektery text item
        if (! startItems.empty() && ! stopItems.empty())
        {
            QGraphicsSimpleTextItem * textitem;
            textitem=qgraphicsitem_cast<QGraphicsSimpleTextItem *>(startItems.first());
            if (textitem)
            {
                startItems.removeFirst();
            }

            textitem=qgraphicsitem_cast<QGraphicsSimpleTextItem *>(stopItems.first());
            if (textitem)
            {
                stopItems.removeFirst();
            }
        }


        if (! startItems.empty() && ! stopItems.empty())
        {


            ItemNode *startItem=qgraphicsitem_cast<ItemNode *>(startItems.first());
            ItemNode *stopItem=qgraphicsitem_cast<ItemNode *>(stopItems.first());

            if (stopItems.count() != 0 && startItems.count() != 0 )
                if (stopItems.first() != startItems.first())
                    if (startItem->itemtype != stopItem->itemtype)
                    {
                        addArc(startItem->item_name,stopItem->item_name,QString(),QString());

                    }
        }
        removeItem(line);
        delete line;


    }
    line=0;
    QGraphicsScene::mouseReleaseEvent(event);
}

/**
 * Nastaveni akce na pozadovanou akci
 * @param act   nova akce
 */
void EditorScene::setAction(Actions act)
{
    action=act;
}

/**
 * Prida graficky objekt "place" do sceny
 * @param pos   pozice na kterou se ma objekt vlozit
 * @param init_ID   identifiktor objektu (v pripade nacitani ze souboru)
 * @param tokens    seznam tokenu (v pripade nacitani ze souboru)
 */
void EditorScene::addPlace(QPointF pos, QString init_ID, QString tokens)
{
    ItemNode * itemNode = new ItemNode (TYPPLACE,ID_P,place_color,this);
    this->addItem(itemNode);
    itemNode->setPos(pos.x(),pos.y());
    saved=false;
    itemNode->setSelected(false);


    if (init_ID.length()!=0)
    {
        itemNode->item_name=init_ID;
        QString str=init_ID;
        str.remove(0,1);
        if (str.toInt() >= ID_P_max)
            ID_P_max=str.toInt();
    }
    else
        ID_P++;

    scenePlaceList->insert(itemNode->item_name,itemNode);


    if (tokens.length()!=0)
    {
        QStringList strlist = tokens.split(",");
        for (int i = 0; i < strlist.size(); ++i)
            itemNode->tokens->append(strlist.at(i).toInt());

    }




    QFont font;
    font.setPixelSize(10);
    itemNode->text_item= new QGraphicsSimpleTextItem(itemNode->item_name,itemNode);
    itemNode->text_item->setFont(font);
    itemNode->text_item->setPos(-5,-18);

    itemNode->token_text_item= new QGraphicsSimpleTextItem(itemNode->str_tokens,itemNode);
    font.setWeight(75);
    font.setFamily("Courier");
    itemNode->token_text_item->setFont(font);
    itemNode->token_text_item->setPos(-((itemNode->str_tokens.length()*6)/2) ,-5);

    itemNode->updateTokenString();

}

/**
 * prida graficky objekt "transition" do sceny
 * @param pos   pozice na kterou se ma objekt vlozit
 * @param init_ID   identifiktor objektu (v pripade nacitani ze souboru)
 * @param init_guard    straz (v pripade nacitani ze souboru)
 * @param init_expr    vystupni vyraz (v pripade nacitani ze souboru)
 */
void EditorScene::addTransition(QPointF pos, QString init_ID, QString init_guard, QString init_expr)
{
    ItemNode * itemNode = new ItemNode (TYPTRANS,ID_T,trans_color,this);
    this->addItem(itemNode);
    itemNode->setPos(pos.x(),pos.y());
    saved=false;
    itemNode->setSelected(false);


    if (init_ID.length()!=0) // v pripade ze se nacita drive ulozena sit
    {
        itemNode->item_name=init_ID;
        QString str=init_ID;
        str.remove(0,1);
        if (str.toInt() >= ID_T_max)
            ID_T_max=str.toInt();
    }
    else
        ID_T++;

    sceneTransitionList->insert(itemNode->item_name,itemNode);



    if (init_guard.length()!=0)
    {
        QStringList strlist = init_guard.split(" & ");
        for (int i = 0; i < strlist.size(); ++i)
            itemNode->guardList->append(strlist.at(i));

    }


    if (init_expr.length()!=0)
    {
        itemNode->str_guardexpr=init_expr;

    }

    itemNode->updateGuardString();


    QFont font;

    font.setPixelSize(10);
    itemNode->text_item= new QGraphicsSimpleTextItem(itemNode->item_name,itemNode);
    itemNode->text_item->setFont(font);
    itemNode->text_item->setPos(0,0);


    font.setWeight(75);
    font.setFamily("Courier");

    itemNode->guard= new QGraphicsSimpleTextItem(itemNode->str_guard,itemNode);
    itemNode->guard->setFont(font);
    itemNode->guard->setPos(0,-15);

    itemNode->guardexpr= new QGraphicsSimpleTextItem(itemNode->str_guardexpr,itemNode);
    itemNode->guardexpr->setFont(font);
    itemNode->guardexpr->setPos(0,5);


    itemNode->updateTransitionSize();

}

/**
 * prida graficky objekt "arc" do sceny
 * @param start_item_name   identifikator objektu ze ktereho hrana vychazi
 * @param stop_item_name   identifikator objektu do ktereho hrana vstupuje
 * @param init_ID   identifiktor objektu (v pripade nacitani ze souboru)
 * @param init_name    nazev (v pripade nacitani ze souboru)
 */
void EditorScene::addArc(QString start_item_name, QString stop_item_name,QString init_ID, QString init_name)
{
    ItemNode *startItem;
    ItemNode *stopItem;

    if (scenePlaceList->contains(start_item_name)) // prvni item je place
    {
        startItem=scenePlaceList->value(start_item_name);
        stopItem=sceneTransitionList->value(stop_item_name);
    }
    else if (scenePlaceList->contains(stop_item_name))
    {
        startItem=sceneTransitionList->value(start_item_name);
        stopItem=scenePlaceList->value(stop_item_name);
    }
    else
    {
        // error
        return;
    }

    if (sceneArcList->contains(startItem->item_name + stopItem->item_name))
        return;


    ItemArc * itemArc = new ItemArc(startItem,stopItem,text_color, this);
    this->addItem(itemArc);  // prida arc na scenu
    itemArc->setZValue(-1);
    saved=false;


    QFont font;
    font.setPixelSize(14);


    if (init_name.length()==0)
        itemArc->arc_name=(QString)ID_A_char;
    else
        itemArc->arc_name=init_name;


    if (init_ID.length()==0)
        itemArc->ID=startItem->item_name + stopItem->item_name;
    else
        itemArc->ID=init_ID;


    itemArc->id_item= new QGraphicsSimpleTextItem(itemArc->arc_name,itemArc);
    itemArc->id_item->setPen(QPen(Qt::black, 1, Qt::SolidLine));
    itemArc->id_item->setBrush(QBrush(Qt::black));

    itemArc->id_item->setFont(font);

    itemArc->ID=startItem->item_name + stopItem->item_name;


    sceneArcList->insert(itemArc->ID,itemArc);

    itemArc->id_item->setPos((itemArc->from->scenePos()+itemArc->to->scenePos())/2);

    ID_A_char++;
    if (ID_A_char == 'z'+1)
        ID_A_char='A';

    if (ID_A_char == 'Z'+1)
        ID_A_char='a';


}

/**
 * jako vychozi identifikatory pro mista a prechody se nastavi maximalni ID ze vsech objektu na scene
 */
void EditorScene::setMaxID()
{
    ID_P=ID_P_max+1;
    ID_T=ID_T_max+1;
}

/**
 * smaze vsechny objekty ze sceny
 */
void EditorScene::deleteAll()
{
    bool empty=false;
    int ret=QMessageBox::Cancel;
    if (!saved)
    {
        QMessageBox msgbox;
        msgbox.setText("The net has been modified.");
        msgbox.setInformativeText("Delete anyway ??");
        msgbox.setStandardButtons(QMessageBox::Ok |  QMessageBox::Cancel);
        ret= msgbox.exec();
    }

    if (saved || ret==QMessageBox::Ok)
    {
        while (!empty)
        {
            QList<QGraphicsItem *> all =items();
            if (all.length() <= 1)
                empty=true;

            if (all.length() != 0)
            {
                QGraphicsItem *item = all.first();
                delete item;
            }
        }
    }

}

/**
 * smaze vsechny vybrane objekty ze sceny
 */
void EditorScene::deleteItem()
{
    bool empty=false;
    bool deleted=false; // smazalo se vubec neco ??
    while (!empty)
    {
        // je potreba pokazde nacist znova vybrane itemy, nektere se totiz mohly mezitim smazat
        QList<QGraphicsItem *> selecteditems = selectedItems();
        if (selecteditems.length() <= 1)
            empty=true;

        if (selecteditems.length()  != 0)
        {
            QGraphicsItem *item = selecteditems.first();

            delete item;
            deleted=true;
        }
    }

    if (deleted) // pokud se neco smazalo
        saved=false;
}

void EditorScene::actualizeTokens(QStringList slist)
{

    foreach (QString str, slist)
    {

        scenePlaceList->value(str.split("=").at(0))->tokens->clear();

        ItemNode *node=scenePlaceList->value(str.split("=").at(0));

        if (str.split("=",QString::SkipEmptyParts).size()>1)
        {
            QStringList sublist=str.split("=").at(1).split(",");

            foreach(QString tok,sublist)
            {

                node->tokens->append(tok.toInt());

            }
        }
        node->updateTokenString();

    }

}

/**
 * Nastaveni vlastnosti vybraneho objektu, zobrazeny dialog se lisi podle typu vybraneho objektu
 */
void EditorScene::editProperties()
{
    QList<QGraphicsItem *> selecteditems = selectedItems(); // nacte vsechny vybrane objekty

    QDialog *dialog= new QDialog(); // novy dialog pro editaci vlastnosti
    dialog->setWindowFlags(Qt::Widget);

    QVBoxLayout *vbox = new QVBoxLayout(dialog);

    node=qgraphicsitem_cast<ItemNode *>(selecteditems.first());

    if (node) // vybrane je misto nebo prechod
    {
        if (node->itemtype == 0) // vybrane je misto
        {
            dialog->setWindowTitle("Properties of place");
            QLabel * label = new QLabel("Name: "+ node->item_name,dialog);
            QLabel * label2 = new QLabel("List of tokens",dialog);

            QListWidget *mylist= new QListWidget(dialog);
            list1 = mylist;


            for (int i=0; i < node->tokens->size();++i)
            {
                QListWidgetItem *item = new QListWidgetItem;
                item->setText(QString::number(node->tokens->at(i)));
                list1->insertItem(0,item);
                list1->setCurrentRow(0);
            }

            QPushButton *b1 = new QPushButton("Add token",dialog);
            QPushButton *b2 = new QPushButton("Remove token",dialog);
            QPushButton *b3 = new QPushButton("Close",dialog);

            vbox->addWidget(label);
            vbox->addWidget(label2);
            vbox->addWidget(list1);
            vbox->addWidget(b1);
            vbox->addWidget(b2);
            vbox->addWidget(b3);

            connect(b1, SIGNAL(clicked()), this, SLOT(addToken()));
            connect(b2, SIGNAL(clicked()), this, SLOT(removeToken()));
            connect(b3, SIGNAL(clicked()), dialog, SLOT(accept()));

            dialog->setLayout(vbox);

            dialog->exec();

        }
        else // vybran je prechod
        {
            dialog->setWindowTitle("Properties of transition");

            QLabel * label1 = new QLabel("Name: "+ node->item_name,dialog);
            QLabel * label2 = new QLabel("List of guards",dialog);
            QLabel * label3 = new QLabel("Expressions:",dialog);

            QListWidget *mylist= new QListWidget(dialog);

            list1 = mylist;

            QLineEdit * lineedit = new QLineEdit(node->str_guardexpr,dialog);

            for (int i=0; i < node->guardList->size();++i)
            {
                QListWidgetItem *item = new QListWidgetItem;
                item->setText(node->guardList->at(i));
                list1->insertItem(0,item);
                list1->setCurrentRow(0);
            }

            QPushButton *b1 = new QPushButton("Add guard",dialog);
            QPushButton *b2 = new QPushButton("Remove guard",dialog);
            QDialogButtonBox *bbox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,Qt::Horizontal,dialog);

            QLabel * label4 = new QLabel("Example: x = y + 5 ; z = 8",dialog);

            vbox->addWidget(label1);
            vbox->addWidget(label2);
            vbox->addWidget(list1);
            vbox->addWidget(b1);
            vbox->addWidget(b2);
            vbox->addWidget(label3);
            vbox->addWidget(label4);
            vbox->addWidget(lineedit);
            vbox->addWidget(bbox);

            if (!node->listOfInputArcs.empty())
                connect(b1, SIGNAL(clicked()), this, SLOT(addGuard()));

            connect(b2, SIGNAL(clicked()), this, SLOT(removeGuard()));

            connect(bbox, SIGNAL(accepted()), dialog, SLOT(accept()));
            connect(bbox, SIGNAL(rejected()), dialog, SLOT(reject()));

            dialog->setLayout(vbox);

            int res=dialog->exec();
            if (res==QDialog::Accepted)
            {
                if (node->str_guardexpr!=lineedit->text())
                {
                    QString expr = lineedit->text();
                    expr.replace(QRegExp("\\s*"), "");

                    if (expr.indexOf(QRegExp("[a-zA-Z][0-9]"))!=-1 ||
                            expr.indexOf(QRegExp("[0-9][a-zA-Z]"))!=-1 ||
                            expr.indexOf(QRegExp("[+-]$"))!=-1 ||
                            expr.indexOf(QRegExp("[=][+-]"))!=-1 ||
                            expr.indexOf(QRegExp("[+][-]"))!=-1 ||
                            expr.indexOf(QRegExp("[-][+]"))!=-1)
                    {
                        // spatny format
                    }
                    else
                    {
                        expr.replace(QRegExp("="), " = ");
                        expr.replace(QRegExp("([+])"), " \\1 ");
                        expr.replace(QRegExp("([-])"), " \\1 ");
                        expr.replace(QRegExp(";"), " ; ");
                        node->str_guardexpr=expr;
                        node->guardexpr->setText(expr);
                        saved=false;
                    }
                }

            }

            node->guard->setText(node->str_guard);

            node->updateTransitionSize();

        }
    }
    else // vybrana je hrana
    {
        dialog->setWindowTitle("Properties of arc");
        ItemArc *arc=qgraphicsitem_cast<ItemArc *>(selecteditems.first());
        if (arc)
        {
            QLabel * label = new QLabel("Name:",dialog);
            QLineEdit * lineedit = new QLineEdit(arc->arc_name,dialog);

            QRegExp rx("([A-Za-z]+)|([-]?[0-9]+)");
            QValidator *validator = new QRegExpValidator(rx, dialog);
            lineedit->setValidator(validator);

            QDialogButtonBox *bbox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,Qt::Horizontal,dialog);

            vbox->addWidget(label);
            vbox->addWidget(lineedit);

            vbox->addWidget(bbox);
            connect(bbox, SIGNAL(accepted()), dialog, SLOT(accept()));
            connect(bbox, SIGNAL(rejected()), dialog, SLOT(reject()));

            dialog->setLayout(vbox);
            int res=dialog->exec();
            if (res==QDialog::Accepted)
            {
                if (lineedit->text()!="-")
                {
                    if (arc->arc_name != lineedit->text())
                    {
                        arc->changeID(lineedit->text());
                        saved=false;
                    }
                }

            }
        }

    }
}

/**
  * Vytvori dialog pro pridavani tokenu do vybraneho mista
  */
void EditorScene::addToken()
{
    QInputDialog * input=new QInputDialog();
    input->setLabelText(tr("Value:"));
    input->setWindowTitle(tr("Add new token"));
    input->setInputMode(QInputDialog::IntInput);
    input->setIntMinimum(INT_MIN);
    input->setIntMaximum(INT_MAX);
    if (input->exec())
        node->tokens->append(input->intValue());

    QListWidgetItem *item = new QListWidgetItem;
    item->setText(QString::number(input->intValue()));
    list1->insertItem(0,item);
    list1->setCurrentRow(0);

    node->updateTokenString();

    saved=false;

}

/**
  * Odebere vybrany token z vybraneho mista
  */
void EditorScene::removeToken()
{
    QListWidgetItem *item = list1->currentItem();

    if (item)
    {
        node->tokens->removeOne(item->text().toInt());
        //list->removeItemWidget(item);
        list1->takeItem(list1->currentRow());
        delete item;
    }
    list1->setCurrentRow(0);

    node->updateTokenString();

    saved=false;

}

/**
  * Vytvori dialog pro pridavani strazi do vybraneho prechodu
  */
void EditorScene::addGuard()
{

    QDialog *dialog= new QDialog();
    dialog->setWindowFlags(Qt::Widget);

    QVBoxLayout *vbox = new QVBoxLayout();
    QHBoxLayout *hbox = new QHBoxLayout();

    QLabel * label = new QLabel("Add guard:",dialog);


    QComboBox * combobox1 = new QComboBox(dialog);
    QStringList stringlist1;
    QComboBox * combobox2 = new QComboBox(dialog);
    QStringList stringlist2;
    QComboBox * combobox3 = new QComboBox(dialog);
    QStringList stringlist3;

    lineedit = new QLineEdit(QString(),dialog);
    lineedit->setFixedWidth(100);

    QRegExp rx("[-]?[0-9]+");
    QValidator *validator = new QRegExpValidator(rx, dialog);
    lineedit->setValidator(validator);


    for (int i = 0; i < node->listOfInputArcs.size(); ++i)
    {
        bool ok;
        node->listOfInputArcs.at(i)->arc_name.toInt(&ok); // pokus prevest nazev hrany na cislo
        if (!ok) //
            stringlist1 << node->listOfInputArcs.at(i)->arc_name;

        node->listOfInputArcs.at(i)->arc_name.toInt(&ok);
        if (!ok)
            stringlist3 << node->listOfInputArcs.at(i)->arc_name;
    }

    combobox1->insertItems(0,stringlist1);

    stringlist2 << "<" << "<=" << ">=" << ">" << "==" << "!=";
    combobox2->insertItems(0,stringlist2);

    stringlist3 << "-CONSTANT-";
    combobox3->insertItems(0,stringlist3);
    if (stringlist3.length()!=1)
        lineedit->setDisabled(true);

    QDialogButtonBox *bbox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,Qt::Horizontal,dialog);

    vbox->addWidget(label);
    vbox->addLayout(hbox);
    vbox->addWidget(bbox);

    hbox->addWidget(combobox1);
    hbox->addWidget(combobox2);
    hbox->addWidget(combobox3);
    hbox->addWidget(lineedit);

    connect(combobox3, SIGNAL(currentIndexChanged(QString)), this, SLOT(addGuardChange(QString)));
    connect(bbox, SIGNAL(accepted()), dialog, SLOT(accept()));
    connect(bbox, SIGNAL(rejected()), dialog, SLOT(reject()));

    dialog->setLayout(vbox);

    int res=dialog->exec();
    if (res==QDialog::Accepted)
    {
        QString str= combobox1->currentText()+" "+combobox2->currentText()+" ";

        if (combobox3->currentIndex() == combobox3->count()-1)
        {
            bool ok;
            int numb=lineedit->text().toInt(&ok);
            if (!ok)
                numb=0;
            str+=QString::number(numb);
        }
        else
            str+=combobox3->currentText();


        if (!node->guardList->contains(str))
        {
            node->guardList->append(str);

            QListWidgetItem *item = new QListWidgetItem;
            item->setText(str);
            list1->insertItem(0,item);
            list1->setCurrentRow(0);

            saved=false;

            if (!node->guardList->empty())
            {
                node->updateGuardString();

            }
        }

    }
}

/**
  * Pomocny slot pro k dialog pro pridavani strazi do vybraneho prechodu
  * Znemoznuje uzivateli zadat nejakou hodnotu v pripade ze neni vybrana moznost "-CONSTANT-"
  * @param id   aktualne vybrane id
  */
void EditorScene::addGuardChange(QString id)
{
    if (id == "-CONSTANT-")
    {
        lineedit->setDisabled(false);
    }
    else
    {
        lineedit->setDisabled(true);
    }

}

/**
  * Odebere vybranou straz z aktualne vybraneho prechodu
  */
void EditorScene::removeGuard()
{
    QListWidgetItem *item = list1->currentItem();

    if (item)
    {
        node->guardList->removeOne(item->text());
        //list->removeItemWidget(item);
        list1->takeItem(list1->currentRow());
        delete item;
    }
    list1->setCurrentRow(0);


    if (!node->guardList->empty())
    {
        node->updateGuardString();

    }
    else
        node->str_guard=QString();

    saved=false;

}

