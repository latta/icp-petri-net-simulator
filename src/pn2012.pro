# -------------------------------------------------
# Project created by QtCreator 2012-03-31T19:26:10
# -------------------------------------------------
QT += core \
    gui
QT += network
QT += xml

TARGET = pn2012
TEMPLATE = app
SOURCES += main.cpp \
    mainwindow.cpp \
    logindialog.cpp \
    connectdialog.cpp \
    xmlparser.cpp \
    editorscene.cpp \
    itemarc.cpp \
    itemnode.cpp \
    openfromserverdialog.cpp
HEADERS += mainwindow.h \
    logindialog.h \
    connectdialog.h \
    xmlparser.h \
    editorscene.h \
    itemarc.h \
    itemnode.h \
    openfromserverdialog.h
FORMS += mainwindow.ui \
    logindialog.ui \
    connectdialog.ui \    
    aboutdialog.ui

RESOURCES += \
    resourceimages.qrc
