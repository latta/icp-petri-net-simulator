/**
 * @file connectdialog.h
 * @author Martin Latta
 * @author Michal Hradecky
 */

#ifndef CONNECTDIALOG_H
#define CONNECTDIALOG_H

#include <QDialog>
#include <QHostAddress>

namespace Ui {
class ConnectDialog;
}

/**
  * Trida reprezentujici dialog pro pripojeni na server
  * @author Martin Latta
  * @author Michal Hradecky
  */
class ConnectDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit ConnectDialog(QWidget *parent = 0);
    ~ConnectDialog();

    void setStatus(QString status);
    QString getServer();
    int getPort();


signals:
    void click_connect();

private slots:
    void on_buttonBox_rejected();
    void on_buttonBox_accepted();

private:
    Ui::ConnectDialog *ui_connect;

};

#endif // CONNECTDIALOG_H
