/**
 * @file editorscene.h
 * @author Martin Latta
 * @author Michal Hradecky
 */

#ifndef EDITORSCENE_H
#define EDITORSCENE_H

#include "itemarc.h"
#include "itemnode.h"
#include <QTime>
#include <QGraphicsScene>
#include <QtCore>
#include <QtGui>

class QGraphicsLineItem;
class ItemNode;
class ItemArc;

#define TYPPLACE 0
#define TYPTRANS 1


/**
  * Trida reprezentujici grafickou scenu do ktere se vkladaji jednotlive graficke objekty (mista, prechody a hrany)
  * @author Martin Latta
  * @author Michal Hradecky
  */
class EditorScene : public QGraphicsScene
{
    Q_OBJECT

public:
    EditorScene(QString tabtitle,QColor *p_color, QColor *t_color, QColor *txt_color);
    ~EditorScene();

    int ID_T;   /**< vychozi identifikator pro novy prechod */
    int ID_P;   /**< vychozi identifikator pro nove misto */

    int ID_T_max;   /**< pri nacitani objektu ze souboru se zde uchova maximalni hodnota identifikatoru */
    int ID_P_max;   /**< pri nacitani objektu ze souboru se zde uchova maximalni hodnota identifikatoru */

    char ID_A_char; /**< vychozi identifikator pro novou hranu */
    bool saved;     /**< priznak sceny jestli se provedla nejaka zmena */

    QColor * place_color;   /**< ukazatel na barvu mista */
    QColor * trans_color;   /**< ukazatel na barvu prechodu */
    QColor * text_color;   /**< ukazatel na barvu textu  */

    enum Actions { ActMove, ActInsPlace, ActInsTrans, ActInsArc};

    QHash<QString, ItemArc *> *sceneArcList;            /**< seznam vsech hran na scene */
    QHash<QString, ItemNode *> *scenePlaceList;         /**< seznam vsech mist na scene */
    QHash<QString, ItemNode *> *sceneTransitionList;    /**< seznam vsech prechodu na scene */


    QString login;          /**< informace z xml - autor */
    QString name;           /**< informace z xml - nazev */
    QString description;    /**< informace z xml - popis */
    int version;            /**< informace z xml - verze */
    QString fileName;       /**< informace z xml - nazev souboru */


    void addArc(QString start_item_name, QString stop_item_name,QString init_ID, QString init_name);
    void addPlace(QPointF pos, QString init_ID,QString tokens);
    void addTransition(QPointF pos, QString init_ID, QString init_guard, QString init_expr);
    void setMaxID();

    void editProperties();
    void deleteAll();
    void deleteItem();

    void actualizeTokens(QStringList slist);

public slots:
    void setAction(Actions action);

    void addToken();
    void removeToken();
    void addGuard();
    void addGuardChange(QString id);
    void removeGuard();

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

private:
    Actions action;    /**< aktualne nastavena akce */

    // pomocne promenne
    QListWidget *list1;
    QLineEdit * lineedit;
    ItemNode *node;
    QGraphicsLineItem * line;

};

#endif // EDITORSCENE_H
