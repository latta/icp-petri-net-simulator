/**
 * @file main.cpp
 * @author Martin Latta
 * @author Michal Hradecky
 */

#include <QCoreApplication>
#include <QHostAddress>
#include <iostream>
#include <cctype>
#include "server.h"

/**
 * Funkce main zaobaluje cely program. Probiha zde kontrola parametru a spousti se poslouchani na uvedenem portu
 * @param argc pocet parametru predanych programu
 * @param argv pole retezcu s parametry predanymi programu
 */
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    
    // v pripade ze neexistuje slozka "nets" na serveru tak se vytvori
    if (!QDir("nets").exists())
        QDir().mkdir("nets");

    int port = 10000;
    QHostAddress addr(QHostAddress::Any);

    // kontrola parametru
    if (argc != 2)    
    {
        std::cerr << "Bad parameters count. Use ./server2012 port_number" << std::endl << std::flush;
        return 1;
    }
    else
    {        
        QString str_port=argv[1];
        port = str_port.toInt();
        if (port==0)
        {
            std::cerr << "Bad port. Use ./server2012 port_number" << std::endl << std::flush;
            return 1;
        }
    }

    Server s(addr, port);

    return a.exec();
}
