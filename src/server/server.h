/**
 * @file server.h
 * @author Martin Latta
 * @author Michal Hradecky
 *
 *
 */

#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QHostAddress>
#include <QTcpServer>
#include <QMutex>
#include "netlist.h"

class QTcpServer;

/**
 * Trida Server dedi od tridy QTcpServer a slouzi k zachytavani novuch spojeni a vytvareni novych vlaken pro obsluhu techto spojeni.
 * @author Martin Latta
 * @author Michal Hradecky
 */
class Server : public QTcpServer
{
    Q_OBJECT

public:
    Server(QHostAddress addr, int port);
    ~Server();

    QHash<QString,QString> * userlist; /**< Asociativni pole uchovavajici uzivatele a jejich hesla. Klic je jmeno uzivatele, hodnota je jeho heslo zakodovane do MD5 */
    NetList * netlist; /**<  seznam informaci o vsech sitich, pristup k ni je vzdy chranen semaforem!!! netlist_mutex */
    QMutex * user_mutex; /**< semafor ktery vylucuje pristup vice uzivatelu k seznamu uzivatelu */
    QMutex * netlist_mutex; /**< semafor ktery vylucuje pristup vice uzivatelu k seznamu siti */

protected:
    virtual void incomingConnection(int socketDescriptor);

private slots:

};


#endif // SERVER_H
