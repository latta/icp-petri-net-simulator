/**
 * @file place.h
 * @author Martin Latta
 * @author Michal Hradecky
 *
 *
 */

#ifndef PLACE_H
#define PLACE_H

#include <QtCore>

/**
 * Trida Place reprezentuje jedno misto v siti
 * @author Martin Latta
 * @author Michal Hradecky
 */
class Place
{
public:
    Place(QString ptokens, QString pname);

    QString name; /**< jmeno mista ve tvaru Pcislo, uzivatel ho nemuze zmenit */
    QList<int> *tokens; /**<  seznam vsech tokenu v tomto miste */
};

#endif // PLACE_H
