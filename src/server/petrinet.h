/**
 * @file petrinet.h
 * @author Martin Latta
 * @author Michal Hradecky
 *
 *
 */

#ifndef PETRINET_H
#define PETRINET_H

#include <QtCore>
#include "place.h"
#include "transition.h"
#include <iostream>

class Place;
class Transition;
class Guard;
class Expression;

/**
 * Trida Petrinet reprezentuje sit jako celek, uchovava vsechna mista a prechody, simulace,...
 * @author Martin Latta
 * @author Michal Hradecky
 */
class Petrinet
{
public:
    Petrinet();
    ~Petrinet();

    void runSimulation(int steps);
    void runOneStep_part1();
    void runOneStep_part2(Transition *trans,QList<int> *tok);
    QString placesToStr();
    QString runableTransToStr();
    QList<int> parseMessage(QString transname,QStringList message);
    QString checkUnboundVariables();

    QHash<QString,Transition *> *translist; /**< asociativni pole ktere uchovava vsechny prechody v siti, klic je jmeno prechodu a hodnoty jsou odkazy na tridu prechodu*/
    QHash<QString,Transition *> *runableTransList; /**< asociativni pole ktere uchovava odpalitelne prechody, klic je jmeno prechodu a hodnoty jsou odkazy na tridu prechodu  */
    QHash<QString,Place *> *places; /**< asociativni pole ktere uchovava vsechny mista v siti, klic je jmeno mista a hodnoty jsou odkazy na tridu mista */
    int maxSteps; /**<  promenna udavajici maximalni pocet kroku pri simulaci naraz */

private:

};

#endif // PETRINET_H
