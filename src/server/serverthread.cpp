/**
 * @file serverthread.cpp
 * @author Martin Latta
 * @author Michal Hradecky
 */

#include <iostream>
#include <QCryptographicHash>
#include "serverthread.h"

/**
 * Zde se do noveho vlakna "privezou" vsechny promenne, ktere jsou sdilena vsemi vlakny
 * @param socketDesc socket deskriptor pomoci ktereho vlakno komunikuje
 * @param usr ukzatel na asociativni pole uzivatelu
 * @param mutex ukazatel na semafor chranici vylucnost prustupu k seznamu uzivatelu
 * @param parent ukazatel na rodice
 * @param pnetlist ukazatel na asociativni pole informaci o sitich ulozenych na serveru
 * @param pmutex ukazatel na semafor chranici vylucnost prustupu k seznamu siti
 */
ServerThread::ServerThread(int socketDesc,QHash<QString, QString> * usr, QMutex * mutex, QObject *parent, NetList *pnetlist, QMutex * pmutex)
    : QThread(parent) // , users(users) //, socketDescriptor(socketDescriptor)
{
    socketDescriptor = socketDesc;
    users=usr;
    user_mutex=mutex;
    netlist_mutex = pmutex;
    netlist = pnetlist;
    pnet = NULL;
}

/**
 * Funkce kontroluje, zda je na serveru uzivatel se jmenem username
 * @param username jmeno zjistovaneho uzivatele
 * @return Vraci true, pokud jiz uzivatel na serveru je, jinak vraci false
 */
bool ServerThread::checkUser(QString username)
{
    bool ret;
    user_mutex->lock();
    ret= (users->contains(username));
    user_mutex->unlock();
    return ret;
}

/**
 * Vytvori uzivatele se jmenem username a heslem psswd, ulozi ho do seznamu uzivatelu a ulozi ji na server
 * @param username jmeno ukladaneho uzivatele
 * @param pwwsd heslo ukladaneho uzivatele
 * @return Vraci true, pokud byl uzivatel vytvoren, pokud jiz existuje, vraci false
 */
bool ServerThread::createUser(QString username,QString psswd)
{
    QString hash_passw = QString(QCryptographicHash::hash(psswd.toUtf8(),QCryptographicHash::Md5).toHex());
    user_mutex->lock();
    if (!users->contains(username))
    {
        users->insert(username,hash_passw);
        if ( ! saveUsersToFile() ) {
            std::cerr << "Users can't be saved to file." << std::endl;
        }
        user_mutex->unlock();
        return true;
    }
    user_mutex->unlock();
    return false;
}

/**
 * Autorizuje prihlaseni uzivatele se jmenem username a heslem psswd
 * @param username jmeno autorizovaneho uzivatele
 * @param pwwsd heslo autorizovaneho uzivatele
 * @return Vraci true, pokud byl uzivatel autorizovan (je zadane spravne jmeno a heslo), kdyz ne, vraci se false
 */
bool ServerThread::autorizeUser(QString username,QString psswd)
{
    bool ret=false;
    QString hash_passw = QString(QCryptographicHash::hash(psswd.toUtf8(),QCryptographicHash::Md5).toHex());
    user_mutex->lock();
    ret = ((users->value(username)) == hash_passw);
    user_mutex->unlock();
    return ret;
}

/**
 * Ulozi seznam uzivatelu z pameti do souboru users v korenovem adresari serveru
 * @return Vraci true, pokud ulozeni probehlo v poradku, pokud ne, vraci se false
 */
bool ServerThread::saveUsersToFile()
{
    // otevreni souboru ./users pro zapis
    QString fileName = "users";
    QFile file(fileName);
    if (!file.open(QFile::WriteOnly | QFile::Text)) {
        std::cerr << "Error: Cannot write file with users "
                  << qPrintable(fileName) << ": "
                  << qPrintable(file.errorString()) << std::endl;
        return false;
    }

    // vytvoreni zakladni xml struktury
    QXmlStreamWriter xmlWriter(&file);
    xmlWriter.setAutoFormatting(true);
    xmlWriter.writeStartDocument();
    xmlWriter.writeStartElement("petri");
    xmlWriter.writeStartElement("users");

    // pridame vsechny uzivatele, pro kazdeho jeden tag <user>;
    QHash<QString, QString>::const_iterator i = users->constBegin();
    while (i != users->constEnd()) {
        xmlWriter.writeStartElement("user");
        xmlWriter.writeAttribute("login", i.key());
        xmlWriter.writeAttribute("password", i.value());
        xmlWriter.writeEndElement();
        ++i;
    }

    // zapisou se vsechny zbyvajici ukoncujici tagy
    xmlWriter.writeEndDocument();

    // zavre se soubor ./users
    file.close();
    if (file.error()) {
        std::cerr << "Error: Cannot write file "
                  << qPrintable(fileName) << ": "
                  << qPrintable(file.errorString()) << std::endl;
        return false;
    }
    return true;
}

/**
 * Funkce zpracovava vsechny prichozi zpravy od klienta a odpovida na ne. Zpravy jsou ve tvaru "COMMAND\nPARAM_1\nPARAM_2"
 * Pokud se neco nepovede, vrati se zprava "FAIL"
 * @param block prichozi zprava od klienta
 * @return Vraci se zprava, ktera se posle zpet klientovi. Zpravy jsou ve tvaru "COMMAND\nPARAM_1\nPARAM_2"
 */
QByteArray ServerThread::handleRequest(QByteArray block)
{
    QByteArray response;
    response.push_back("ERROR");
    QList<QByteArray> msg = block.split('\n');
    if (msg.isEmpty())
        return response;

    if (msg.last().data()==QString("EOF"))
        msg.removeLast();


    // podle toho jaky je pozadavek od klienta se rozhodne co se bude delat
    QString command = msg.takeFirst();

    if (command == "LOGIN") // pozadavek na prihlaseni
    {
        if (msg.count()<2) return response;
        response.clear();
        if (!checkUser(msg.first().data()))
        {
            // pokud uzivatel na serveru neni
            response.push_back("DOESNTEXIST");
        }
        else
        {
            if (!autorizeUser(msg.first().data(),msg.last().data()))
            {
                // klient poslal spatne heslo
                response.push_back("WRONGPASSWORD");
            }
            else
            {
                // klient je autorizovan
                user=msg.first().data();
                response.push_back("OK");
            }
        }
        return response;
    }
    else if (command == "NEWUSER") // pozadavek na vytvoreni noveho uzivatele
    {
        if (msg.count()<2) return response;
        response.clear();
        if (createUser(msg.first().data(),msg.last().data()))
            response.push_back("OK");
        else
            response.push_back("ALREADYEXISTS");

        return response;
    }
    else if (command == "SAVE_PREPARE") //pozadavek na ulozeni souboru se siti, probehne kontrola
    {
        //if (msg.count()<4) return response;


        netlist_mutex->lock();
        response.clear();

        // zpracovani parametru zpravy
        QString login = msg.at(0).data();
        QString name = msg.at(1).data();
        QString vers_str = msg.at(2).data();
        int vers = vers_str.toInt();
        QString desc = msg.at(3).data();
        //std::cout << "pozadavek na ulozeni site " << qPrintable(name) << " " << qPrintable(desc) << std::endl << std::flush;

        // pridani site do seznamu na serveru
        lastFileName = "nets/"+name+"_"+vers_str+".pn";

        // kontrola zda jiz verze na serveru neni
        if (netlist->exists(name,vers))
        {
            int new_version=netlist->getNextFreeVersion(name);
            response.push_back("ALREADY_EXISTS\n"+QByteArray::number(new_version));
            //std::cout << "jiz existuje, volna je verze " << new_version << std::endl << std::flush;
            netlist_mutex->unlock();
            return response;
        }

        // ulozeni site do seznamu siti a na disk
        if (netlist->addNet(name,vers,name+"_"+vers_str+".pn",login,desc))
        {
            //netlist->saveToFile();
            response.push_back("OK");
            //std::cout << "siti neexistuje, posilej " << std::endl << std::flush;
        }
        else
        {
            netlist_mutex->unlock();
            response.push_back("FAIL");
        }

        return response;
    }
    else if (command == "SAVE") // s timto pozadavkem se jiz posila samotny soubor
    {
        response.clear();

        block.remove(block.lastIndexOf("\nEOF"),4);

        // otevreni souboru pro zapis
        QFile file(lastFileName);
        if (!file.open(QFile::WriteOnly | QFile::Text)) {
            std::cerr << "Error: Cannot write file "
                      << qPrintable(lastFileName) << ": "
                      << qPrintable(file.errorString()) << std::endl;
            response.push_back("FAIL");
            netlist_mutex->unlock();
            return response;
        }

        // zapsani souboru na disk
        file.write(block.mid((command+"\n").length()));
        file.close();
        if (file.error()) {
            std::cerr << "Error while closing file "
                      << qPrintable(lastFileName) << ": "
                      << qPrintable(file.errorString()) << std::endl;
            response.push_back("FAIL");
            netlist_mutex->unlock();
            return response;
        }

        //std::cout << "ulozen soubor " << qPrintable(lastFileName) << std::endl << std::flush;
        response.push_back("OK");
        // zmeny se zapisi do souboru se seznamem siti na disku
        if ( !netlist->saveToFile() )
        {
            std::cerr << "Error: Cannot write file with netlist, changes are only temporaly";
        }

        netlist_mutex->unlock();
        return response;
    }
    else if (command == "LIST_NAMES") // pozadavek na jmena vsech siti na serveru
    {
        response.clear();
        netlist_mutex->lock();
        response.push_back(netlist->getNames().join("\n").toAscii());
        netlist_mutex->unlock();
    }
    else if (command == "LIST_VERSIONS") // pozadavek na vssechny verze jedne site
    {
        if (msg.empty()) return response;
        response.clear();
        netlist_mutex->lock();
        response.push_back(netlist->getVersions(msg.at(0).data()).join("\n").toAscii());
        netlist_mutex->unlock();
    }
    else if (command == "LIST_DETAILS") //pozadavek na detaily site dane jmenem a verzi
    {
        if (msg.count()<2) return response;
        QString vers_str = msg.at(1).data();
        int vers = vers_str.toInt();

        response.clear();
        netlist_mutex->lock();
        response.push_back(netlist->getDetails(msg.at(0).data(),vers).join("\n").toAscii());
        netlist_mutex->unlock();
    }
    else if (command == "LIST_RUNS") // pozadavek na ziskani vsech simulaci jedne site dane jmenem
    {
        if (msg.empty()) return response;
        response.clear();
        QString name = msg.at(0).data();

        netlist_mutex->lock();
        response.push_back("RUNS\n");
        response.push_back(netlist->getRuns(name)->join("\n").toAscii());
        netlist_mutex->unlock();
    }
    else if ((command == "RUN_SIMULATION") || (command == "RUN_STEP")) // pozadavek na simulaci
    {
        //if (msg.count()<2) return response;

        response.clear();
        delete pnet;
        pnet = new Petrinet();

        QString vers_str = msg.at(1).data();
        int vers = vers_str.toInt();

        netlist_mutex->lock();
        netlist->addCurrentRun(msg.at(0).data(),user);
        netlist->saveToFile();
        QString fileName = netlist->getDetails(msg.at(0).data(),vers)[0];
        //std::cout << "Simulate file " << qPrintable(fileName) << std::endl << std::flush;
        netlist_mutex->unlock();

        // nacteni souboru souboru se siti
        XmlParserServer net_reader(pnet);
        if ( ! net_reader.readFile("nets/"+fileName) )
        {
            std::cerr << "invalid petri net file."<<std::endl << std::flush;
            response.push_back("FAIL");
            return response;
        }

        // hledani nenavazanych promennych
        QString uv= pnet->checkUnboundVariables();
        if (uv.length()!=0)
        {
            response.push_back("UNBOUNDVARIABLE\n");
            response.push_back(uv.toAscii());
            return response;
        }

        if (command == "RUN_SIMULATION") // spusteni simulace naraz
        {
            pnet->runSimulation(pnet->maxSteps);
            response.push_back("RUN_OK\n");
            //std::cout << qPrintable(pnet->placesToStr()) << std::endl;
            response.push_back(pnet->placesToStr().toAscii());
        }
        else // spusteni jednoho kroku simulace
        {
            response.clear();

            // spusteni prvni casti simulace
            pnet->runOneStep_part1();

            if (pnet->runableTransList->isEmpty()) // jiz nejde nic simulovat
            {
                response.push_back("NOTRANS");
            }
            else // jeste je co simulovat, posleme klientovi na vyber
            {
                response.push_back("CHOOSETRANS\n");
                response.push_back(pnet->runableTransToStr().toAscii());
            }
        }

    }
    else if (command == "RUN_STEP_P2") // klient nam odpovedel ktery prechod a jake tokeny chce odpalit
    {
        //if (msg.count()<2) return response;
        response.clear();
        QString pom = msg.at(0).data();
        QStringList parts = pom.split(";");
        QString name=parts.at(0);
        parts = parts.at(1).split(",");
        QList<int> list = pnet->parseMessage(name,parts);

        pnet->runOneStep_part2(pnet->runableTransList->value(name),&list);

        response.push_back("STEP_OK\n");
        response.push_back(pnet->placesToStr().toAscii());
    }
    else if (command == "OPEN_NET") // pozadavek na otevreni site ze serveru
    {
        if (msg.empty()) return response;
        response.clear();

        QString fileName="nets/";
        fileName.append(msg.at(0).data());

        // otevreni souboru - kdyz se nepovede, tak konec
        QFile file(fileName);
        if (!file.open(QFile::ReadOnly | QFile::Text)) {
            std::cerr << "Can't open file' "
                      << qPrintable(fileName) << ": "
                      << qPrintable(file.errorString()) << std::endl;
            response.append("FAIL");
            return response;
        }

        response = file.readAll();
        response.push_front("FILE\n");
        response.push_back("\nEOF");

    }

    return response;
}


/**
 * Redefinovana funkce z tridy QThread, bezi v ni vlakno pro jedno sezeni klienta
 */
void ServerThread::run()
{
    QTcpSocket tcpSocket;

    // nastavime socketu socket deskriptor
    if (!tcpSocket.setSocketDescriptor(socketDescriptor))
    {
        std::cerr << "Can't set socket descriptor in thread" << std::endl << std::flush;
        return;
    }

    // pockame se az se spojime
    tcpSocket.waitForConnected(-1);
    disconnected=false;

    QByteArray block_in,block_out;

    // komunikujeme dokud jsme pripojeni
    while (!disconnected)
    {

        block_in.clear();

        tcpSocket.waitForReadyRead(-1);

        block_in=tcpSocket.readAll();

        while (!block_in.contains("\nEOF"))
        {
            tcpSocket.waitForReadyRead(-1);

            block_in.append(tcpSocket.readAll());

        }

        block_out.clear();
        block_out=handleRequest(block_in);

        tcpSocket.write(block_out);

        if (tcpSocket.state()==QAbstractSocket::UnconnectedState)
            disconnected=true;

    }
}
