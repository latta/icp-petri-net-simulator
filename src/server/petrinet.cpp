/**
 * @file petrinet.cpp
 * @author Martin Latta
 * @author Michal Hradecky
 *
 */

#include "petrinet.h"

/**
 * Alokovani mista pro vsechny promenne a nastaveni maximalniho poctu kroku simulace
 */
Petrinet::Petrinet()
{
    translist = new QHash<QString,Transition *>;
    places = new  QHash<QString,Place *>;
    runableTransList = new QHash<QString,Transition *>;
    maxSteps = 50; // vykona se max 50 kroku simulace
}

/**
 * uvolneni pameti vsech naalokovanych promennych
 */
Petrinet::~Petrinet()
{
    delete translist;
    delete runableTransList;
    delete places;
}


/**
 * Spusti naraz simulaci a vysledek ulozi do promenne runableTranslist.
 * Provadi se dokud jsou nejake proveditelne prechody nebo se neprovede maximalni pocet kroku
 * @param steps udava kolik kroku simulace se provede
 */
void Petrinet::runSimulation(int steps)
{
    do
    {
        // prvni cast simluce, hledaji se tu vsechny proveditelne prechody
        runOneStep_part1();

        // pokud uz neni mozny zadny prechod, tak se skonci
        if (runableTransList->isEmpty()) break;

        // vyber tokenu, pri jednorazove simulaci zvolime prvni sadu z prvniho prechodu
        Transition *trans = runableTransList->values().first();
        QList<int> *tok = trans->possibleCombinations->first();

        //std::cout << qPrintable(runableTransToStr()) << std::endl;

        // aplikovani vyrazu a oddeleni prislusnych tokenu ze zdrojovych mist
        runOneStep_part2(trans,tok);

        steps--;
        //std::cout << "steps left" << steps << std::endl;
    } while ( steps>0 && !runableTransList->empty() );
}

/**
 * Spusti prvni cast simulace. Zde se najdou vsechny proveditelne prechody (vyhovuji strazim) a ulozi se do promenne runableTranslist
 */
void Petrinet::runOneStep_part1()
{
    runableTransList->clear();

    // projiti vsech prechodu a zjisteni, ktere jsou proveditelne
    foreach(const QString &key, translist->keys())
    {
        Transition *trans;
        trans=translist->value(key);
        trans->allCombinations->clear();
        trans->possibleCombinations->clear();
        QList<int> tmp;
        trans->getAllCombinations(trans->fromPlaces.length()-1,tmp);

        // vypis vsech moznosti
        /*
        std::cout << "Vsechny sady tokenu z mist:" << std::endl;
        QList<QList<int>*>::iterator i=trans->allCombinations->begin();
        for (;i<trans->allCombinations->end();++i)
        {
            QList<int>::iterator j=(*i)->begin();
            for(;j< (*i)->end();++j)
            {
                std::cout << (*j) <<", ";
            }
            std::cout << std::endl;
        }
        */

        // ze vsech kombinaci se vyberou jen ty proveditelne
        trans->getPosibleCombinations();

        // vypis vsech mist co vedou do prechodu
        /*
        std::cout << "-------------------" << std::endl;
        std::cout << "Proveditelne sady tokenu" << std::endl;
        QStringList::iterator str_iter=trans->fromPlaces.begin();
        for (;str_iter < trans->fromPlaces.end();++str_iter)
        {
            std::cout  << qPrintable((*str_iter)) << ", ";
        }
        std::cout << std::endl;


        // vypis vsech moznosti
        QList<QList<int>*>::iterator iter=trans->possibleCombinations->begin();
        for (;iter<trans->possibleCombinations->end();++iter)
        {
            QList<int>::iterator j=(*iter)->begin();
            for(;j< (*iter)->end();++j)
            {
                std::cout << (*j) <<", ";
            }
            std::cout << std::endl;
        }
        */


        // pridani do listu vsech proveditelnych prechodu
        if (!trans->possibleCombinations->isEmpty())
        {
            runableTransList->insert(key,trans);
            //std::cout << qPrintable(key) << " je proveditleny" << std::endl;
        }
        else
        {
            // zastaveni uz nepujde do dalsiho kroku

        }

    }
}


/**
 * Spusti druhou cast simulace. Prechod a sada tokenu byla vybrana vybrana uzivatelem/prvni mozna.
 * Vymazou se vybrane tokeny a provedou se vystuni vyrazy
 * @param trans Vybrany prechod, ktery se ma odpalit
 * @param tok sada tokenu ktera se pouzije pri odpaleni
 */
void Petrinet::runOneStep_part2(Transition *trans,QList<int> *tok)
{
    // provedeni vyrazu u prechodu
    QList<Expression*>::iterator i=trans->exprList->begin();
    for (;i<trans->exprList->end();++i)
    {
        Place* place= places->value((*i)->dest);
        int soucet=0;
        int int1=0;
        int op=1;

        QStringList::iterator j=(*i)->explist.begin();
        for (;j<(*i)->explist.end();++j)
        {
            if ((*j)[0]=='P')
                int1 = tok->at(trans->fromPlaces.indexOf(*j));
            else if ((*j)=="+"){
                op=1;
                continue;}
            else if ((*j)=="-"){
                op=-1;
                continue;}
            else if ((*j).indexOf(QRegExp("^[-]?\\d+$"))!=-1)
                int1=(*j).toInt();
            //else
            //std::cout << "chybny vyraz" << qPrintable((*j)) << std::endl;

            soucet += (op*int1);

        }
        //std::cout << qPrintable((*i)->dest) << " += " << soucet << std::endl;
        place->tokens->append(soucet);
    }

    // odebrani tokenu z mist
    QStringList::iterator it= trans->fromPlaces.begin();
    for (;it<trans->fromPlaces.end();++it)
    {
        Place* place= places->value((*it));
        place->tokens->removeOne(tok->at(trans->fromPlaces.indexOf((*it))));

    }

}

/**
 * Projde sit a vyhleda v ni promenne, ktere neodkazuji na zadne misto
 * @return vrati prvni nevazanou promennou, kdyz zadna neexistuje, tak se vrati prazdny retezec
 */
QString Petrinet::checkUnboundVariables()
{

    // prochazi se vsechny prechody
    foreach(const QString &key, translist->keys())
    {

        Transition *trans;

        trans=translist->value(key);

        // hledame nevazane promenne ve vyrazech
        QList<Expression*>::const_iterator iter2;
        for( iter2 = trans->exprList->begin(); iter2 != trans->exprList->end(); ++iter2 )
        {

            if (((*iter2)->dest.indexOf(QRegExp("^[a-zA-Z]+$")))!=-1)
            {
                return (*iter2)->dest;
            }
            QStringList::iterator i= (*iter2)->explist.begin();
            for (;i< (*iter2)->explist.end();++i)
            {
                if ((*i).indexOf(QRegExp("^[a-zA-Z]+$"))!=-1)
                {
                    return *i;
                }
            }

        }

        // hledame nevazane promenne ve strazich
        QList<Guard*>::const_iterator iter;
        for( iter = trans->guardList->begin(); iter != trans->guardList->end(); ++iter )
        {
            if ((*iter)->place1.indexOf(QRegExp("^[a-zA-Z]+$"))!=-1)
            {
                return (*iter)->place1;
            }

            if ((*iter)->place2.indexOf(QRegExp("^[a-zA-Z]+$"))!=-1)
            {
                return (*iter)->place2;
            }

        }
    }

    return QString();
}


/**
 * Vytvori retezec pro poslani klientovi s vyberem vsech proveditelnych prechodu T a sad tokenu z kazdeho mista P
 * retezec ma format "T1;P1=2,P2=3;P1=2,P2=3\nT2;P3=4"
 * @return vrati retezec obsahujici vsechny proveditelne prechy a u kazdeho sady jejich tokenu, ktere vyhovuji strazim
 */
QString Petrinet::runableTransToStr()
{
    QString result="";

    // projdeme vsechny proveditelne prechody
    QHash<QString,Transition *>::iterator i=runableTransList->begin();
    for (;i != runableTransList->end();++i)
    {
        //std::cout << "prechod: " << qPrintable((*i)->name) << std::endl;
        result.append((*i)->name);

        // projdeme vsechny pripustne sady tokenu
        QList<QList<int>*>::iterator j = (*i)->possibleCombinations->begin();
        for (;j<(*i)->possibleCombinations->end();++j)
        {
            result.append(";");
            for (int k=0;k<(*j)->count();k++)
            {
                QString place_name = (*i)->fromPlaces.at(k);
                result.append(place_name);
                result.append("=");
                result.append(QString::number((*j)->at(k)));
                if (k!=(*j)->count()-1) result.append(",");
            }
        }
        if ((*i)->fromPlaces.empty()) result.chop(1);

        result.append("\n");
    }
    if (!runableTransList->empty()) result.chop(1);

    return result;
}

/**
 * Vytvori retezec pro poslani klientovi se seznamem vsech places P a jejich hodnot
 * retezec ma format "P1=1,2,3\nP2=5,6"
 * @return vrati retezec obsahujici prechody
 */
QString Petrinet::placesToStr()
{
    QString result=QString();

    // projdeme vsechny mista
    QHash<QString,Place *>::iterator i=places->begin();
    for (;i !=places->end();++i)
    {
        //std::cout << "place " << qPrintable((*i)->name) << std::endl;
        result.append((*i)->name);
        result.append("=");

        // projdeme vsechny tokeny z daneho mista
        QList<int>::iterator j= (*i)->tokens->begin();
        for (;j<((*i)->tokens->end());++j)
        {
            result.append(QString::number((*j)));
            result.append(",");
            //std::cout << "   token " << (*j) << std::endl;
        }

        if (!(*i)->tokens->empty()) result.chop(1);

        result.append("\n");
    }
    if (!places->empty()) result.chop(1);
    return result;
}

/**
 * Zpracuje zpravu od klienta ve ktere si klient vibira prechod a tokeny k odpaleni. Je to odpoved na retezec ziskany z runableTransToStr()
 * @param transname jmeno prechodu, ktery si klient vybral
 * @param message vybrana sada tokenu
 * @return vrati seznamu cisel s vybranymi tokeny ve spravnem poradi
 */
QList<int> Petrinet::parseMessage(QString transname,QStringList message)
{
    QList<int> intlist;

    QStringList::iterator i=message.begin();
    for (;i<message.end();++i)
    {
        intlist.append(0);
    }

    i=message.begin();
    for (;i<message.end();++i)
    {
        QStringList slist=(*i).split("=");
        intlist.insert((runableTransList->value(transname)->fromPlaces.indexOf(slist[0])),slist[1].toInt());
    }

    return intlist;
}

