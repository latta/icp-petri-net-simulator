/**
 * @file serverthread.h
 * @author Martin Latta
 * @author Michal Hradecky
 */

#ifndef SERVERTHREAD_H
#define SERVERTHREAD_H

#include <QThread>
#include <QTcpSocket>
#include <QAbstractSocket>
#include <QtNetwork>
#include <QByteArray>
#include <QObject>
#include <QDataStream>
#include <QHash>

#include <QMutex>
#include "netlist.h"
#include "petrinet.h"
#include "xmlparserserver.h"

class Petrinet;

/**
 * Trida ServerThread zajistuje chod vlakna, komunikuje s klientem a vyrizuje jeho pozadavky
 * @author Martin Latta
 * @author Michal Hradecky
 */
class ServerThread : public QThread
{
    Q_OBJECT

public:
    ServerThread(int socketDescriptor,QHash<QString, QString> *usr, QMutex * mutex, QObject *parent, NetList *pnetlist, QMutex * pmutex);
    void run();
    QByteArray handleRequest(QByteArray block);

private:
    bool checkUser(QString username);
    bool createUser(QString username,QString psswd);
    bool autorizeUser(QString username,QString psswd);
    bool saveUsersToFile();

    bool disconnected;
    int socketDescriptor;
    QString user; /**< retezec s loginem prihlasneho uzivatele */
    QHash<QString, QString> * users; /**< Asociativni pole uchovavajici uzivatele a jejich hesla. Klic je jmeno uzivatele, hodnota je jeho heslo zakodovane do MD5 */
    NetList * netlist; /**<  seznam informaci o vsech sitich, pristup k ni je vzdy chranen semaforem!!! netlist_mutex */
    QMutex * user_mutex; /**< semafor ktery vylucuje pristup vice uzivatelu k seznamu uzivatelu */
    QMutex * netlist_mutex; /**< semafor ktery vylucuje pristup vice uzivatelu k seznamu siti */
    QString lastFileName; /**< jmeno souboru do ktereho se ulozi sit pri ukladani */
    Petrinet * pnet; /**< uchovava vsechna mista a prechody, simulace,... */
};

#endif // SERVERTHREAD_H
