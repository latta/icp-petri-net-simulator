/**
 * @file place.cpp
 * @author Martin Latta
 * @author Michal Hradecky
 *
 *
 */

#include "place.h"

/**
 * Alokovani mista pro vsechny promenne a ulozeni pocatecnich hodnot
 * @param ptokens retezec s tokeny ktere jsou maji byt ulozeny
 * @pname retezec se jmenem mista
 */
Place::Place(QString ptokens, QString pname)
{
    tokens = new QList<int>;
    name =pname;

    // zpracujeme vstupni retezec a vysledky ulozime do lokalnich struktur
    if (ptokens.length()!=0)
    {
        QStringList str_tokens = ptokens.split(", ");

        // kazdy nalezeny token ulozime
        QStringList::iterator i = str_tokens.begin();
        for (;i<str_tokens.end();++i)
        {
            tokens->append((*i).toInt());
        }
    }

}
