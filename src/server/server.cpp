/**
 * @file server.cpp
 * @author Martin Latta
 * @author Michal Hradecky
 *
 *
 */

#include <iostream>
#include <cstdio>

#include <QtNetwork>
#include <QTcpServer>
#include <QTcpSocket>
#include <QThread>
#include <qxml.h>
#include <QXmlStreamWriter>
#include <QHash>

#include "server.h"
#include "serverthread.h"
#include "xmlparserserver.h"
#include "netlist.h"

/**
 * Alokovani mista pro vsechny promenne a ulozeni pocatecnich hodnot. Po probehnuti jiz server posloucha na uvedenem portu.
 * @param port udava port, na kterem server nasloucha
 * @param addr udava adresy na kterych server posloucha
 */
Server::Server(QHostAddress addr, int port)
{

    userlist = new QHash<QString,QString>;
    netlist = new NetList;

    user_mutex = new QMutex;
    netlist_mutex = new QMutex;

    // nacteni souboru s uzivateli
    user_mutex->lock();
    XmlParserServer user_reader(userlist);
    if ( ! user_reader.readFile("users") )
    {
        std::cerr << "invalid file with users."<<std::endl << std::flush;
        exit(EXIT_FAILURE);
    }
    user_mutex->unlock();


    // nacteni souboru se seznamem siti
    netlist_mutex->lock();
    XmlParserServer netlist_reader(netlist);
    if ( ! netlist_reader.readFile("netlist") )
    {
        std::cerr << "invalid file with net list."<<std::endl << std::flush;
        exit(EXIT_FAILURE);
    }
    netlist_mutex->unlock();


    // maximalne muze byt pripojeno 50 klientu
    this->setMaxPendingConnections(50);

    // zkusime poslouchat na adrese a portu
    if (!this->listen(addr, port))
    {
        std::cerr << "can't listen on port " << port << std::endl << std::flush;
        exit(EXIT_FAILURE);
    }

}

/**
 * Reimplmetovana metoda z QTcpServer; zalozi nove vlaskno a preda mu socket descriptor
 * @param socketdescriptor socketdesckriptor, ktery se preda nove vytvorenemu vlaknu
 */
void Server::incomingConnection(int socketDescriptor)
{
    //std::cout << "prichozi spojeni\n" << std::flush;
    ServerThread *thread = new ServerThread(socketDescriptor, userlist, user_mutex, this,netlist,netlist_mutex);
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    thread->start();
}

/**
 * Uvolneni pameti vsech naalokovanych promennych
 */
Server::~Server()
{
    delete userlist;
    delete netlist;
    delete user_mutex;
    delete netlist_mutex;
}
