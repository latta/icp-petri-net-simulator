/**
 * @file xmlparserserver.cpp
 * @author Martin Latta
 * @author Michal Hradecky
 *
 *
 */

#include <iostream>
#include "xmlparserserver.h"

/**
 * Ulozeni ukazatelu z rodice
 * @param usr ukazatel na seznam uzivatelu
 */
XmlParserServer::XmlParserServer(QHash<QString,QString> *usr)
{
    users = usr;
    netlist = NULL;
    net = NULL;
}

/**
 * Ulozeni ukazatelu z rodice
 * @param pnetlist ukazatel na seznam siti
 */
XmlParserServer::XmlParserServer(NetList *pnetlist)
{
    netlist = pnetlist;
    users = NULL;
    net = NULL;
}

/**
 * Ulozeni ukazatelu z rodice
 * @param pnet ukazatel sit
 */
XmlParserServer::XmlParserServer(Petrinet *pnet)
{
    net = pnet;
    users = NULL;
    netlist = NULL;
}

/**
 * Otevre a zpracuje Xml soubor
 * @param finaName jmeno souboru ktery se ma zpracovat
 * @return vraci true, pokud se vse nacetlo v poradku, jinak vraci false
 */
bool XmlParserServer::readFile(const QString &fileName)
{
    QFile file(fileName);
    if (!file.open(QFile::ReadOnly | QFile::Text)) {
        std::cerr << "File can't be open..." << std::endl;
        return false;
    }
    reader.setDevice(&file);

    reader.readNext();
    while (!reader.atEnd())
    {
        if (reader.isStartElement())
        {
            if (reader.name() == "petri")
            {
                readPetriElement();
            }
            else
            {
                reader.raiseError(QObject::tr(" is not a valid petri file"));
            }
        }
        else
        {
            reader.readNext();
        }
    }

    file.close();
    if (reader.hasError())
    {
        std::cerr << "Error in parsing file"  << std::endl;
        return false;
    } else if (file.error() != QFile::NoError) {
        std::cerr << "Error in reading file"  << std::endl;
        return false;
    }
    return true;
}


/**
 * Zpracovani jednoho elementu z nacteneho Xml souboru
 */
void XmlParserServer::readPetriElement()
{
    reader.readNext();
    while (!reader.atEnd()) {
        if (reader.isEndElement()) {
            reader.readNext();
            break;
        }

        if (reader.isStartElement()) {
            if (reader.name() == "net") {
                readNetElement();
            }
            else if (reader.name() == "users") {
                readUsersElement();
            }
            else if (reader.name() == "netlist") {
                    readNetlistElement();
            }
            else
            {
                reader.raiseError(QObject::tr(" invalid petri net file. unknown tag ")+reader.name().toString());
                reader.readNext();
            }
        } else {
            reader.readNext();
        }
    }

}

/**
 * Zpracovani jednoho elementu z nacteneho Xml souboru
 */
void XmlParserServer::readNetElement()
{

    // tagy run, place, arc, transition jsou ve tvaru <run x="y" /> , maji za sebou imaginarni zaviraci tag
    reader.readNext();
    while (!reader.atEnd()) {
        if (reader.isEndElement()) {
            reader.readNext();
            break;
        }

        if (reader.isStartElement() ) {
            if (reader.name() == "transition") {
                readTransitionElement();
            } else if (reader.name() == "arc") {
                readArcElement();
            } else if (reader.name() == "place") {
                readPlaceElement();
            } else {
                reader.raiseError(QObject::tr(" invalid petri net file. unknown tag ")+reader.name().toString());
                reader.readNext();
            }
        } else {
            reader.readNext();
        }
    }

}

/**
 * Zpracovani jednoho elementu z nacteneho Xml souboru
 */
void XmlParserServer::readUsersElement()
{
    reader.readNext();
    while (!reader.atEnd()) {
        if (reader.isEndElement()) {
            reader.readNext();
            break;
        }

        if (reader.isStartElement() ) {
            if (reader.name() == "user") {
                readUserElement();
            } else {
                reader.raiseError(QObject::tr(" invalid user file. unknown tag ")+reader.name().toString());
                reader.readNext();
            }
        } else {
            reader.readNext();
        }
    }
}

/**
 * Zpracovani jednoho elementu z nacteneho Xml souboru
 */
void XmlParserServer::readPlaceElement()
{
    QString id=reader.attributes().value("id").toString();

    Place *newplace = new Place(reader.attributes().value("tokens").toString(),id);
    net->places->insert(id,newplace);

    // musi byt dvakrat kvuli imaginarnimu zaviracimu tagu
    reader.readNext();
    reader.readNext();
}

/**
 * Zpracovani jednoho elementu z nacteneho Xml souboru
 */
void XmlParserServer::readTransitionElement()
{
    QString id = reader.attributes().value("id").toString();
    QString guards = reader.attributes().value("guards").toString();
    QString expression = reader.attributes().value("expression").toString();

    Transition *newtrans = new Transition(net,id);
    newtrans->addGuards(guards);
    newtrans->addExpression(expression);
    net->translist->insert(id,newtrans);

    // musi byt dvakrat kvuli imaginarnimu zaviracimu tagu
    reader.readNext();
    reader.readNext();
}

/**
 * Zpracovani jednoho elementu z nacteneho Xml souboru
 */
void XmlParserServer::readArcElement()
{    
    QString start = reader.attributes().value("start").toString();
    QString stop = reader.attributes().value("stop").toString();
    QString id = reader.attributes().value("id").toString();
    QString name = reader.attributes().value("name").toString();

    if (id=="")
    {
        reader.raiseError(QObject::tr(" invalid petri net file. unknown tag ")+reader.name().toString());
    }
    else
    {
        Transition *trans;
        if (id[0]=='T') // kdyz vychazi z prechodu
        {
            trans=net->translist->value(start);
            bool ok;
            name.toInt(&ok);
            if (ok)
            {
                Expression *e=new Expression(name);
                e->dest = stop;
                trans->exprList->append(e);
            }
            else
            {
                QList<Expression*>::const_iterator iter2;
                for( iter2 = trans->exprList->begin(); iter2 != trans->exprList->end(); ++iter2 )
                {
                    if ((*iter2)->dest==name)
                    {
                        (*iter2)->dest = stop;
                    }
                }
            }
        }
        else if (id[0]=='P') // kdyz vede do prechodu
        {
            trans=net->translist->value(stop);

            // pridani do seznamu mist ktere vedou do prechodu
            net->translist->value(stop)->fromPlaces.append(start);

            bool ok;
            name.toInt(&ok);
            if (ok)
            {
                Guard *g=new Guard(start+" == "+name);
                trans->guardList->append(g);
            }

            else

            {
                // nahrazeni ve vsech strazich
                QList<Guard*>::const_iterator iter;
                for( iter = trans->guardList->begin(); iter != trans->guardList->end(); ++iter )
                {
                    if ((*iter)->place1==name)
                    {
                        (*iter)->place1 = start;
                    }

                    if ((*iter)->place2==name)
                    {
                        (*iter)->place2 = start;
                    }

                }

                // nahrazeni ve vsech pravych stranach vyrazu
                QList<Expression*>::const_iterator iter3;
                for( iter3 = trans->exprList->begin(); iter3 != trans->exprList->end(); ++iter3 )
                {
                    QStringList::iterator i= (*iter3)->explist.begin();
                    for (;i< (*iter3)->explist.end();++i)
                    {

                        if ((*i)==name)
                        {

                            *i=start;
                        }
                    }

                }
            }

        }
    }

    // musi byt dvakrat kvuli imaginarnimu zaviracimu tagu
    reader.readNext();
    reader.readNext();
}

/**
 * Zpracovani jednoho elementu z nacteneho Xml souboru
 */
void XmlParserServer::readUserElement()
{

    QString user = reader.attributes().value("login").toString();
    QString pass = reader.attributes().value("password").toString();

    // vlozime uzivatele do seznamu
    users->insert(user,pass);

    // musi byt dvakrat kvuli imaginarnimu zaviracimu tagu
    reader.readNext();
    reader.readNext();
}

/**
 * Zpracovani jednoho elementu z nacteneho Xml souboru
 */
void XmlParserServer::readNetlistElement()
{
    reader.readNext();
    while (!reader.atEnd()) {
        if (reader.isEndElement()) {
            reader.readNext();
            break;
        }

        if (reader.isStartElement() ) {
            if (reader.name() == "netname") {
                readNetnameElement();
            } else {
                reader.raiseError(QObject::tr(" invalid netlist file. unknown tag ")+reader.name().toString());
                reader.readNext();
            }
        } else {
            reader.readNext();
        }
    }

}

/**
 * Zpracovani jednoho elementu z nacteneho Xml souboru
 */
void XmlParserServer::readNetnameElement()
{
    QString name = reader.attributes().value("name").toString();

    reader.readNext();
    while (!reader.atEnd()) {
        if (reader.isEndElement()) {
            reader.readNext();
            break;
        }

        if (reader.isStartElement() ) {
            if (reader.name() == "version") {
                readVersionElement(name);
            } else if (reader.name() == "run") {
                readRunElement(name);
            } else {
                reader.raiseError(QObject::tr(" invalid netlist file. unknown tag ")+reader.name().toString());
                reader.readNext();
            }
        } else {
            reader.readNext();
        }
    }
}


/**
 * Zpracovani jednoho elementu z nacteneho Xml souboru
 * @param netname jmeno site z rodicovskeho elementu
 */
void XmlParserServer::readVersionElement(QString netname)
{
    QString fileName = reader.attributes().value("filename").toString();
    QString desc = reader.attributes().value("description").toString();
    QString author = reader.attributes().value("author").toString();
    QString vers = reader.attributes().value("number").toString();
    int version=vers.toInt();

    if (!netlist->addNet(netname,version,fileName,author,desc))
        reader.raiseError(QObject::tr("net wasn't added - ")+netname+tr(" verze ")+vers);

    // musi byt dvakrat kvuli imaginarnimu zaviracimu tagu
    reader.readNext();
    reader.readNext();
}

/**
 * Zpracovani jednoho elementu z nacteneho Xml souboru
 * @param netname jmeno site z rodicovskeho elementu
 */
void XmlParserServer::readRunElement(QString netname)
{
    QString login = reader.attributes().value("login").toString();
    QString date = reader.attributes().value("date").toString();
    QString time = reader.attributes().value("time").toString();

    if (!netlist->addRun(netname,login,date,time))
        reader.raiseError(QObject::tr("run wasn't added - ")+netname+tr(" time ")+time);

    // musi byt dvakrat kvuli imaginarnimu zaviracimu tagu
    reader.readNext();
    reader.readNext();
}
