/**
 * @file transition.h
 * @author Martin Latta
 * @author Michal Hradecky
 */

#ifndef TRANSITION_H
#define TRANSITION_H

#include <QtCore>
#include "petrinet.h"

class Petrinet;


/**
 * Trida Guard reprezentuje jednu straz v prechodu siti
 * @author Martin Latta
 * @author Michal Hradecky
 */
class Guard
{
public:
    Guard(QString oneGuard);

    QString place1; /**< jmeno mista prvniho operandu */
    int op; /**< index operatoru */
    QString place2; /**< jmeno mista druheho operandu */
    int constant; /**< hodnota konstany, kdyz je 2. operand konstanta*/
    bool isconst; /**< true - 2.operand je konstanta , false - 2.operand je misto */
};

/**
 * Trida Expression reprezentuje jeden vyraz v prechodu siti
 * @author Martin Latta
 * @author Michal Hradecky
 */
class Expression
{
public:
    Expression(QString oneExpr);

    QString dest; /**< misto kam se bude ukladat vysledek vyrazu */
    QStringList explist; /**< seznam vyrazu v danem prechodu */

};


class Transition
{
public:
    Transition(Petrinet *pnet,QString pname);

    void addGuards(QString guards);
    void addExpression(QString pexp);
    void getAllCombinations(int index,QList<int> intlist); // da do allCombinations vsechny kombinace tokenu
    void getPosibleCombinations(); // da do possibleCombinations jen proveditelne kombinace tokenu
    bool checkGuards(QList<int>* intlist); // vraci zda hodnoty v listu vyhovuji vsem strazim
    bool checkGuard(Guard *g,QList<int> *intlist); // vraci zda hodnoty v listu vyhovuji dane strazi

    QList<Guard*> *guardList;  /**< seznam strazi v prechodu */
    QList<Expression*> *exprList; /**< seznam vyrazu v prechodu */
    Petrinet *net; /**< ukazatel na sit ve ktere se prechod nachazi */
    QStringList fromPlaces; /**< seznam places ktere jsou do prechodu */
    QList<QList<int>*> *allCombinations;  /**<  vsechny mozne kombinace tokenu z tohoto prechodu*/
    QList<QList<int>*> *possibleCombinations;  /**< proveditelne kombinace tokenu z tohoto prechodu*/

    QString name;  /**< jmeno prechodu */



};

#endif // TRANSITION_H
