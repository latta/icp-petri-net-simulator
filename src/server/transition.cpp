/**
 * @file transition.cpp
 * @author Martin Latta
 * @author Michal Hradecky
 */

#include "transition.h"

/**
 * Alokovani mista pro vsechny promenne a ulozeni pocatecnich hodnot.
 * @param pnet ukazatel na sit
 * @param pname unikatni jmeno prechodu, nelze zmeit uzivatelem
 */
Transition::Transition(Petrinet *pnet, QString pname)
{
    guardList = new QList<Guard*>;
    exprList = new QList<Expression*>;
    allCombinations = new QList<QList<int>*>;
    possibleCombinations = new QList<QList<int>*>;
    net=pnet;
    name=pname;
}

/**
 * Prida vice straze do prechodu, straze jsou od sebe oddeleny " & "
 * @param guards retezec se strazemi
 */
void Transition::addGuards(QString guards)
{

    // rozdeli jednotlive straze
    if (guards == "") return;
    QStringList strlist = guards.split(" & ");

    // prochazime jednotlivymi podretezci a pridavame je do seznamu strazi
    QStringList::iterator i=strlist.begin();
    for (;i != strlist.end();++i)
    {
        Guard * g= new Guard(*i);
        guardList->append(g);
    }

}

/**
 * Prida vice vyrazu do prechodu, vyrazy jsou od sebe oddeleny " ; "
 * @param pexp retezec se strazemi
 */
void Transition::addExpression(QString pexp)
{

    if (pexp=="") return;

    if (pexp.length()!=0)
    {
        // rozdelime retezec na jednotlive vyrazy
        QStringList strlist = pexp.split(" ; ");

        // projdeme jednotlive podretezce a zpracujeme je a pridame je do seznamu
        QStringList::iterator i=strlist.begin();
        for (;i != strlist.end();++i)
        {
            QStringList strlist2 = (*i).split(" = ");

            if (strlist2.count() == 2)
            {
                Expression * e= new Expression(strlist2[1]);
                e->dest=strlist2[0];
                exprList->append(e);
            }
        }
    }
}


/**
 * funkce ziskava vsechny mozne kombinace tokenu, ktere pro ktere je dany prechod prveditelny a ulozi je do allCombinations
 * funkce rekurzivne prochazi postupne prochazi vsechna mista
 * @param index urcuje konec rekurze
 * @param do tohoto seznamu se v kazde urovni rekurze prida jeden token a na konci rekurze se ulozi do seznamu
 */
void Transition::getAllCombinations(int index,QList<int> intlist)
{
    if (fromPlaces.empty()) return;
    QList<int> *tok=net->places->value(fromPlaces[index])->tokens;

    // prochazime vsechny tokeny v miste
    QList<int>::iterator i=tok->begin();
    for (; i<tok->end();++i)
    {
        intlist.prepend((*i));

        // koncova podminka rekurze
        if (index == 0)
        {
            // jsme na nejvetsi hlobce - v intlist je jiz cela posloupnost
            QList<int> *newlist = new QList<int>;
            *newlist=intlist;
            allCombinations->append(newlist);
        }
        else // we must go deeper
        {
            getAllCombinations(index-1,intlist);
        }
        intlist.removeFirst();
    }
}

/**
 * funkce zpracuje seznam allCombinations, ktery naplnila funkce getAllCombinations a do possibleCombinations ulozi jen
 * sady tokenu, ktere vyhovuji strazim v danem prechodu
 */
void Transition::getPosibleCombinations()
{
    QList<QList<int>*>::iterator i=allCombinations->begin();
    for (;i<allCombinations->end();++i)
    {
        if (checkGuards((*i)))
        {
            QList<int> *newlist= new QList<int>;
            *newlist = *(*i);
            possibleCombinations->append(newlist);
        }
    }
}

/**
 * funkce kontroluje, zda sada tokenu v seznamu intlist vyhovuje vsem strazim danem prechodu
 * @param intlist sada tokenu ke kontrole
 * @return vrati true, pokud sada vhovuje vsem strazim, jinak se vraci false
 */
bool Transition::checkGuards(QList<int> *intlist)
{
    bool ok=true;
    QList<Guard*>::iterator i = guardList->begin();
    for( ; i != guardList->end(); ++i )
    {
        ok= ok && checkGuard((*i),intlist);
    }
    return ok;
}

/**
 * funkce kontroluje, zda sada tokenu v seznamu intlist vyhovuje strazi g v danem prechodu
 * @param intlist sada tokenu ke kontrole
 * @param g straz, ktera se kontroluje
 * @return vrati true, pokud sada vyhovuje strazi g, jinak se vraci false
 */
bool Transition::checkGuard(Guard *g,QList<int> *intlist)
{

    // zadame cisla co se budou porovnavat
    int int1=intlist->at(fromPlaces.indexOf(g->place1));
    int int2;
    if (g->isconst)
    {
        int2 = (g->constant);
    }
    else
        int2=intlist->at(fromPlaces.indexOf(g->place2));

    //std::cout << std::endl << "porovnani " << int1 << " " << g->op << " " <<  int2 << std::endl;

    // samotna operace porovnani
    switch (g->op)
    {
    case 0: //
        if ((int1) < (int2)){
            return true;}
        break;
    case 1: //
        if ((int1) <= (int2))
            return true;
        break;
    case 2: //
        if ((int1) >= (int2))
            return true;
        break;
    case 3: //
        if ((int1) > (int2))
            return true;
        break;
    case 4: //
        if ((int1) == (int2))       
            return true;
        break;
    case 5: //
        if ((int1) != (int2))
            return true;
        break;
    }
    return false;
}


/**
 * Zpracovani vyrazu a ulozeni pocatecnich hodnot.
 * @param oneExpr retezec s vyrazem k pridani
 */
Expression::Expression(QString oneExpr)
{
    // rozdelime retezec s vyrazem podle " "
    QStringList strlist = oneExpr.split(" ");
    if (strlist.count()!=0)
    {
        // vsechny podretezce pridame do seznamu
        QStringList::iterator i=strlist.begin();
        for (;i != strlist.end();++i)
        {
            explist << *i;
        }

    }
}

/**
 * Zpracovani straze a ulozeni pocatecnich hodnot.
 * @param oneExpr retezec se strazi k pridani
 */
Guard::Guard(QString oneGuard)
{
    place1=QString();
    place2=QString();
    op=0;
    constant=0;

    QStringList oplist;
    oplist << "<" << "<=" << ">=" << ">" << "==" << "!=";

    // rozdelime si straz na operand operator a operand a ulozime je
    QStringList strlist = oneGuard.split(" ");
    if (strlist.count()==3)
    {
        place1=strlist[0];
        op= oplist.indexOf(strlist[1]);

        bool ok;
        constant=strlist[2].toInt(&ok);
        if (ok)
            isconst=true;
        else
        {
            isconst=false;
            place2=strlist[2];
        }
    }
}
