#-------------------------------------------------
#
# Project created by QtCreator 2012-04-04T11:40:02
#
#-------------------------------------------------

QT += core
QT += network
QT += xml
QT -= gui

TARGET = server2012
CONFIG   += console
#CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    server.cpp \
    serverthread.cpp \
    xmlparserserver.cpp \
    netlist.cpp \
    petrinet.cpp \
    place.cpp \
    transition.cpp

HEADERS += server.h \
    serverthread.h \
    xmlparserserver.h \
    netlist.h \
    petrinet.h \
    place.h \
    transition.h
