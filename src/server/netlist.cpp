/**
 * @file netlist.cpp
 * @author Martin Latta
 * @author Michal Hradecky
 *
 * Soubor s metodami tridy NetList
 */

#include "netlist.h"


/**
 * Alokovani mista pro vsechny promenne
 */
NetList::NetList()
{
    names = new QHash<QString, TVersions*>;
    runs = new QHash<QString, TRunDetails*>;
}


/**
 * Prida zaznam o nove siti do asociativniho pole names,
 * @param name jmeno pridavane site
 * @param version verze pridavane site
 * @param fileName jmeno souboru ve kterem je pridavana sit ulozena
 * @param author jmeno autora pridavane site
 * @param description popis pridavane site
 * @return kdyz sit zvoleneho jmena a verze jiz existuje, vrati se false, jinak se vrati true
 */
bool NetList::addNet(QString name,int version, QString fileName, QString author,QString description)
{
    if (! names->contains(name))
    {
        TVersions *versions = new TVersions;
        names->insert(name,versions);
    }

    if (names->value(name)->contains(version))
    {
        //std::cerr << "netlist jiz obsahuje sit " << qPrintable(name) << " verze " << version << std::endl << std::flush;
        return false;
    }
    else
    {
        QStringList values;
        values.append(fileName);
        values.append(author);
        values.append(description);
        names->value(name)->insert(version,values);
        //std::cout << "pridana sit " << qPrintable(name) << " verze " << version << " popis " << qPrintable(description) << std::endl << std::flush;
        return true;
    }

}

/**
 * Zjistuje zda existije sit name a verze version
 * @param name jmeno hledane site
 * @param version verze hledane site
 * @return Vraci true, pokud existuje sit jmena name a verze version
 */
bool NetList::exists(QString name, int version)
{
    if ( ! names->contains(name) ) return false;
    if ( ! names->value(name)->contains(version) ) return false;
    return true;
}

/**
 * Vraci true, pokud existuje sit jmena name
 * @param name jmeno hledane site
 * @return Vraci true, pokud existuje sit jmena name
 */
bool NetList::exists(QString name)
{
    if ( ! names->contains(name) ) return false;
    return true;
}

/**
 * Vraci cislo o 1 vyzsi nez je cislo nejvyzsi ulozene verze site s jmenem @name
 * @param name jmeno hledane site
 */
int NetList::getNextFreeVersion(QString name)
{
    // kdyz tam neni, tak se vrati 1
    int next = 0;
    if ( ! exists(name) ) return ++next;

    // ziskame nejvyzsi cislo verze
    TVersions::iterator i = names->value(name)->begin();
    for (; i != names->value(name)->end(); ++i)
    {
        if (i.key() > next)
            next = i.key();
        //std::cout << i.key() << ": " << i.value() << endl;
    }

    // vrati o 1 vyzsi nez je zatim nejvyzsi obsazene
    return ++next;
}

/**
 * Ulozi aktualni zaznamy o sitich do souboru ./netlist v korenovem adresari serveru.
 * @return Vraci true, kdyz se zapis povede, jinak vrati false
 */
bool NetList::saveToFile()
{
    QString fileName = "netlist";
    QFile file(fileName);
    if (!file.open(QFile::WriteOnly | QFile::Text)) {
        std::cerr << "Error: Cannot write file "
                  << qPrintable(fileName) << ": "
                  << qPrintable(file.errorString()) << std::endl;
        return false;
    }

    QXmlStreamWriter xmlWriter(&file);
    xmlWriter.setAutoFormatting(true);
    xmlWriter.writeStartDocument();
    xmlWriter.writeStartElement("petri");
    xmlWriter.writeStartElement("netlist");

    // hledani a zapsani vsech <netname>
    QHash<QString, TVersions*>::const_iterator i = names->constBegin();
    while (i != names->constEnd()) {
        xmlWriter.writeStartElement("netname");
        xmlWriter.writeAttribute("name", i.key());

        // vyhledani a zapsani vsech <version>
        TVersions::iterator j = names->value(i.key())->begin();
        for (; j != names->value(i.key())->end(); ++j)
        {
            xmlWriter.writeStartElement("version");
            xmlWriter.writeAttribute("number", QString::number(j.key()));
            xmlWriter.writeAttribute("filename", j.value()[0]);
            xmlWriter.writeAttribute("author", j.value()[1]);
            xmlWriter.writeAttribute("description", j.value()[2]);
            xmlWriter.writeEndElement();
        }

        // vyhledani a zapsani vsech <run>
        if (runs->contains(i.key()))
        {
            TRunDetails::iterator k = runs->value(i.key())->begin();
            for (; k != runs->value(i.key())->end(); ++k)
            {
                xmlWriter.writeStartElement("run");
                xmlWriter.writeAttribute("login", (*k)[0]);
                xmlWriter.writeAttribute("date", (*k)[1]);
                xmlWriter.writeAttribute("time", (*k)[2]);
                xmlWriter.writeEndElement();
            }
        }

        xmlWriter.writeEndElement();
        ++i;
    }

    xmlWriter.writeEndDocument();
    file.close();
    if (file.error()) {
        std::cerr << "Error: Cannot write file "
                  << qPrintable(fileName) << ": "
                  << qPrintable(file.errorString()) << std::endl;
        return false;
    }
    return true;
}

/**
 * @return Vraci seznam jmen vsech siti ulozenych na serveru
 */
QStringList NetList::getNames()
{
    QStringList data;
    foreach(const QString &key, names->keys())
        data << key;
    return data;
}

/**
 * @param name jmeno site pro kterou vracime verze
 * @return Vraci seznam vsech verzi site name ulozenych na serveru
 */
QStringList NetList::getVersions(QString name)
{
    QStringList data;
    if (exists(name))
    {
        foreach(const int &key, names->value(name)->keys())
            data << QString::number(key);

    }
    return data;
}


/**
 * @param name jmeno site pro kterou vracime verze
 * @param version verze site se jmenem name pro kterou vracime detaily
 * @return Vraci QStringList, [0]=fileName [1]=autor [2]=popis
 */
QStringList NetList::getDetails(QString name,int version)
{
    QStringList data;
    if (exists(name,version))
    {
        //std::cout << qPrintable(names->value(name)->value(version)[2]) << std::endl << std::flush;
        return names->value(name)->value(version);
    }
    return data;
}

/**
 * Prida zaznam o nove simulaci do pameti serveru
 * @param name jmeno site pridavane simulace
 * @param login jmeno uzivatele ktery sit simuloval
 * @param date datum kdy simulace probehla
 * @param time cas kdy simulace probehla
 * @return kdyz jiz existuje simulace zvoleneho jmena site, datumu a casu vrati se false, jinak se vrati true
 */
bool NetList::addRun(QString name, QString login,QString date, QString time)
{
    if (! runs->contains(name))
    {
        TRunDetails *rundetail = new TRunDetails;
        runs->insert(name,rundetail);
    }

    QStringList values;
    values << login << date << time;

    if (runs->value(name)->contains(values))
    {
        //std::cerr << "netlist jiz obsahuje danou simulaci " << qPrintable(name) << " cas " << qPrintable(time) << std::endl << std::flush;
        return false;
    }
    else
    {
        runs->value(name)->append(values);
        //std::cout << "pridana simulace " << qPrintable(name) << " cas " << qPrintable(time) << std::endl << std::flush;
        return true;
    }
}

/**
 * Prida zaznam o nove simulaci do pameti serveru v aktualni cas
 * @param name jmeno pridavane site
 * @param login jmeno uzivatele ktery sit simuloval
 */
void NetList::addCurrentRun(QString name, QString login)
{
    QString date = QDate::currentDate().toString();
    QString time = QTime::currentTime().toString();
    addRun(name,login,date,time);
}

/**
 * Vyhleda vsechny zaznamy o simulacich site name
 * @param name jmeno site pridavane simulace
 * @return vrati seznam retezcu, kde v retezci [0]=autor [1]=datum [2]=cas
 */
QStringList *NetList::getRuns(QString name)
{
    QStringList *data = new QStringList;
    if (runs->contains(name))
    {
        QList<QStringList>::iterator i=runs->value(name)->begin();
        for (;i!=runs->value(name)->end();++i)
        {
            data->append( (*i).join("\t"));
        }

    }
    return data;
}
