/**
 * @file xmlparserserver.h
 * @author Martin Latta
 * @author Michal Hradecky
 */

#ifndef XMLPARSERSERVER_H
#define XMLPARSERSERVER_H

#include <qxml.h>
#include <QXmlStreamReader>
#include <QObject>
#include <QHash>
#include "netlist.h"
#include "petrinet.h"

/**
 * Trida Xml parser obsahuje metody ke zpracovani vsech pouzitych typu Xml souboru
 * Algoritmus zpracovani Xml dokumentu je inspirovan kapitolou 16 z knihy
 * C++ GUI Programming with Qt4, 2nd Edition by Jasmin Blanchette, Mark Summerfield
 * (www.informit.com/articles/article.aspx?p=1405553)
 * @author Martin Latta
 * @author Michal Hradecky
 */
class XmlParserServer : public QObject
{
    Q_OBJECT

public:
    explicit XmlParserServer(QHash<QString,QString> *usr);
    explicit XmlParserServer(NetList *pnetlist);
    explicit XmlParserServer(Petrinet *pnet);
    bool readFile(const QString &fileName);

private:
    void readPetriElement();

    // <net> soubor s periho siti
    void readNetElement();
    void readPlaceElement();
    void readTransitionElement();
    void readArcElement();
    void readRunElement(QString netname);

    // <users> soubor s ulozenymi uzivateli
    void readUsersElement();
    void readUserElement();

    // <netlist> soubor se seznamem siti
    void readNetlistElement();
    void readNetnameElement();
    void readVersionElement(QString netname);

    void skipUnknownElement();

    QXmlStreamReader reader; /**< instance xml ctecky */
    QHash<QString,QString> *users; /**< ukazatel na seznam uzivatelu do ktereho se budou nactene informace nacitat */
    NetList *netlist; /**< ukazatel na seznam siti do ktereho se informace budou nacitat */
    Petrinet *net; /**< ukazatel na siti do ktere se nactene informace budu nacitat */

};
#endif // XMLPARSERSERVER_H
