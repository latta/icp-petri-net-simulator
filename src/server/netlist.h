/**
 * @file netlist.h
 * @author Martin Latta
 * @author Michal Hradecky
 *
 */

#ifndef NETLIST_H
#define NETLIST_H

#include <string>
#include <iostream>
#include <QObject>
#include <QtCore>

/** @typedef TVersions definuje asociativni seznam, kde klic je verze site a hodnota je seznam retezcu s detaily a siti */
typedef QHash<int,QStringList> TVersions;
/** @typedef TRunDetails definuje seznam hodnot typu QStringList */
typedef QList<QStringList> TRunDetails;

/**
 * Trida NetList uchovava informace o sitich, ktere jsou louzeny na serveru a poskytuje je ostatnim
 * @author Martin Latta
 * @author Michal Hradecky
 */
class NetList
{
public:
    NetList();

    bool addNet(QString name,int version, QString fileName, QString author,QString description);
    bool exists(QString name);
    bool exists(QString name, int version);
    int getNextFreeVersion(QString name);
    bool saveToFile();
    bool addRun(QString name, QString login,QString date, QString time);
    void addCurrentRun(QString name, QString login);
    QStringList getNames();
    QStringList getVersions(QString name);
    QStringList getDetails(QString name,int version);
    QStringList *getRuns(QString name);

private:
    QHash<QString,TVersions*> * names; /**< asocitivni pole detaily o vsech sitich, klicem je unikatni jmeno site, hodnota obsahuje odkaz na asocitivni pole vsech verzi*/
    QHash<QString,TRunDetails*> *runs; /**< asociativni pole na uchovani zaznamu o simulacich, klic je unikatni jmeno site, hodota je odkaz seznam vsech simulaci dane site*/

};

#endif // NETLIST_H
