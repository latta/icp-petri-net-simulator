/**
 * @file itemnode.cpp
 * @author Martin Latta
 * @author Michal Hradecky
 */

#include "itemnode.h"

/**
 * Nastaveni vychozich hodnot
 * @param typ objektu (misto nebo prechod)
 * @param id identifikator (ma smysl pouze v pripade nacitani ze souboru)
 * @param color ukazatel na barvu objektu
 * @param s scena na kterou se graficky objekt vaze
 *
 */
ItemNode::ItemNode(int typ, int id, QColor * color, EditorScene *s)
{
    itemtype=typ;
    scene=s;
    fillColor=color;

    str_tokens=QString();
    str_guard=QString();
    str_guardexpr=QString();

    if (itemtype == TYPPLACE)
    {
        ID=id;
        item_name="P"+QString::number(id);
        tokens= new QList<int>;
        guardList =NULL;
    }
    else
    {
        ID=id;
        item_name="T"+QString::number(id);
        t_size_min=80;
        t_size=80;
        guardList = new QList<QString>;
        tokens=NULL;
    }

    setFlag(QGraphicsItem::ItemIsMovable, true);
    setFlag(QGraphicsItem::ItemIsSelectable, true);
    setFlag(QGraphicsItem::ItemSendsScenePositionChanges,true);
}

/**
 * V destruktoru je potreba smazat vsechny hrany ktere jsou napojene na objekt
 */
ItemNode::~ItemNode()
{
    ItemArc *arc;
    foreach (arc, listOfInputArcs)
    {
        if (arc != NULL)
            delete arc;
    }
    foreach (arc, listOfOutputArcs)
    {
        if (arc != NULL)
            delete arc;
    }

    if (itemtype==TYPPLACE)
        scene->scenePlaceList->remove(item_name);
    else
        scene->sceneTransitionList->remove(item_name);


    delete tokens;
    delete guardList;

//    delete listOfInputArcs;
//    delete listOfOutputArcs;
}

/**
  * Aktualizuje retezec a textovy objekt nesouci tokeny
  */
void ItemNode::updateTokenString()
{
    if (!tokens->empty())
    {
        str_tokens.clear();
        str_tokens.append(QString::number(tokens->first()));
        // tokeny spojene carkou se vlozi do retezce
        QList<int>::iterator i;
        for (i=tokens->begin()+1; i!=tokens->end(); ++i)
            str_tokens.append(","+ QString::number(*i));
    }
    else
        str_tokens=QString();

    // nastavi spravnou delku a vycentrovani textoveho objektu
    token_text_item->setText(str_tokens);
    token_text_item->setPos(-((str_tokens.length()*6)/2) ,-5);
}

/**
  * Aktualizuje retezec a textovy objekt nesouci jednotlive straze
  */
void ItemNode::updateGuardString()
{
    if (!guardList->empty())
    {
        str_guard.clear();
        str_guard.append(guardList->first());
        QList<QString>::iterator i;
        for (i=guardList->begin()+1; i!=guardList->end(); ++i)
            str_guard.append(" & "+ *i);
    }
}

/**
  * Prizpusobi velikost prechodu podle delky retezce se strazemi a vystupnimi vyrazy
  */
void ItemNode::updateTransitionSize()
{

    int len1=0, len2=0;

    if (str_guardexpr.length()!=0)
    {
        // delka retezce s vystupnimi vyrazy
        len1=(str_guardexpr.length()+1)*6+8;
        // textovy objekt se vycentruje
        guardexpr->setPos(-((str_guardexpr.length()*6)/2) ,5);
    }

    if (str_guard.length()!=0)
    {
        // delka retezce se strazemi
        len2=str_guard.length()*6+8;
        // textovy objekt se vycentruje
        guard->setPos(-((str_guard.length()*6)/2) ,-15);
    }

    // jako aktualni sirka prechodu se nastavi vetsi
    t_size= (len1 > len2) ? len1 : len2;

    // sirka nesmi byt mensi nez minimalni sirka
    t_size= (t_size > t_size_min) ? t_size : t_size_min;

    text_item->setPos(-t_size/2+2,-5);

}

/**
  * Reimplementovana metoda na nastaveni hranic grafickeho objektu
  * Objekt "misto" ma konstantni hranici, objekt "prechod" ma dynamicky menici se
  */
QRectF ItemNode::boundingRect() const
{
    if (this->itemtype==TYPPLACE)
        return QRectF(-20,-20,40,40);
    else
        return QRectF(-(t_size/2)-1,-20,t_size+1,40);

}

/**
  * Pridani hrany do seznamu vstupnich nebo vystupnich hran
  * @param arc  pridavana hrana
  */
void ItemNode::addArc(ItemArc *arc)
{

    if (itemtype == TYPTRANS)
    {
        if (arc->from->itemtype == TYPTRANS)
        {
            listOfOutputArcs.append(arc);
        }
        else
        {
            listOfInputArcs.append(arc);
        }

    }
    else
    {
        if (arc->from->itemtype == TYPPLACE)
        {
            listOfOutputArcs.append(arc);
        }
        else
        {
            listOfInputArcs.append(arc);
        }
    }

}

/**
  * Odebrani hrany ze seznamu vstupnich nebo vystupnich hran
  * @param arc  odebirana hrana
  */
void ItemNode::removeArc(ItemArc *arc)
{
    if (listOfInputArcs.contains(arc) && arc !=NULL)
    {
        listOfInputArcs.removeOne(arc);
    }
    else if (listOfOutputArcs.contains(arc) && arc !=NULL)
    {
        listOfOutputArcs.removeOne(arc);
    }

}

/**
 * Reimplementovana metoda na vykresleni grafickeho objektu
 */
void ItemNode::paint(QPainter *painter, const QStyleOptionGraphicsItem * option, QWidget *widget)
{
    Q_UNUSED(option)
    Q_UNUSED(widget)

    QFont font;
    font.setPixelSize(10);
    painter->setFont(font);

    if (this->itemtype == TYPTRANS)
    {
        painter->setBrush(QBrush(fillColor->rgb()));
        painter->drawRect(-(t_size/2),-19,t_size,38);
        painter->drawLine(-(t_size/2)+4+(text_item->text().length()*6),0,(t_size/2)-4,0);
    }
    else // typ PLACE
    {
        painter->setBrush(QBrush(fillColor->rgb()));
        painter->drawEllipse(-20,-20,40,40);
    }

    // pokud je objekt vybrany, nastavi se sedy carkovany obrys
    if (this->isSelected())
    {
        painter->setBrush(Qt::NoBrush);
        painter->setPen(QPen(Qt::gray, 1, Qt::DotLine));
        painter->drawRect(boundingRect());
    }
}

/**
 * Reimplementovana metoda k zachyceni zmeny objektu
 */
QVariant ItemNode::itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value)
{
    // pokud se objekt posune tak se scena nastavi na neulozenou
    if (change== ItemPositionHasChanged)
    {

        scene->saved=false;
    }

    return QGraphicsItem::itemChange(change, value);
}
