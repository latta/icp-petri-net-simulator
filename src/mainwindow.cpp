/**
 * @file mainwindow.cpp
 * @author Martin Latta
 * @author Michal Hradecky
 */

#include "logindialog.h"
#include "ui_logindialog.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "connectdialog.h"
#include "ui_connectdialog.h"
#include "xmlparser.h"
#include "openfromserverdialog.h"
#include "ui_aboutdialog.h"

#include <QAbstractSocket>
#include <QtGui>
#include <QtNetwork>
#include <QMessageBox>
#include <QTcpSocket>
#include <QDataStream>
#include <QByteArray>
#include <QFileDialog>
#include <QtCore>
#include <iterator>

/**
  * Inicializace hlavniho okna
  */
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QCoreApplication::setApplicationName("pn2012");
    setWindowTitle(qAppName()+ " - not saved");

    //nacteni velikosti a pozice okna z nastaveni
    readSettings();

    // inicializace a nastaveni hodnot pro config
    p_color= new QColor(Qt::red);
    t_color= new QColor(Qt::yellow);
    txt_color= new QColor(Qt::green);

    config_list= new QList<QStringList>;
    current_style = new int;
    *current_style=0;

    // nacteni configuracniho souboru
    XmlParser style_reader(config_list,current_style);
    style_reader.readFile("config.xml");

    // inicializace toolbaru a tabu
    setupToolbar();
    ui->tabWidget->removeTab(0);
    ui->tabWidget->removeTab(0);
    addNewTab("new_petri_net");
    changeStyle(*current_style);

    tempCount = 0;

    // nastaveni status baru
    server_status_label = new QLabel();
    user_status_label = new QLabel();
    statusBar()->insertPermanentWidget(0,server_status_label,100);
    statusBar()->insertPermanentWidget(1,user_status_label,100);
    server_status_label->setText("Disconnected");
    user_status_label->setText("---");


    socket = new QTcpSocket(this);

    // napojeni tlacitek v hlavnim menu na prislusne sloty
    connect(ui->actionConnect, SIGNAL(activated()),this,SLOT(openConnectDialog()));
    connect(ui->actionLogout, SIGNAL(activated()),this,SLOT(doDisconnect()));
    connect(ui->actionOpen, SIGNAL(activated()),this,SLOT(openNetLocal()));
    connect(ui->actionSave, SIGNAL(activated()),this,SLOT(saveNet()));
    connect(ui->actionSave_as, SIGNAL(activated()),this,SLOT(saveNetAs()));
    connect(ui->actionOpen_from_server, SIGNAL(activated()),this,SLOT(openNetFromServer()));
    connect(ui->actionSave_on_server, SIGNAL(activated()),this,SLOT(saveNetOnServer()));
    connect(ui->actionClose, SIGNAL(activated()),this,SLOT(closeCurrentTab()));
    connect(ui->actionNew, SIGNAL(triggered()), this, SLOT(addNewTabSlot()));
    connect(ui->actionAbout, SIGNAL(triggered()), this, SLOT(aboutDialog()));

    // napojeni tabu na prislusne sloty
    connect(ui->tabWidget, SIGNAL(currentChanged(int)), this, SLOT(tabSwitched()));
    connect(ui->tabWidget, SIGNAL(currentChanged(int)), this, SLOT(toolGroupClicked()));
    connect(ui->tabWidget,SIGNAL(tabCloseRequested(int)),this,SLOT(closeTab(int)));

    // napojeni socketu na prislusne sloty
    connect(socket, SIGNAL(connected()),this,SLOT(openLoginDialog()));
    connect(socket, SIGNAL(disconnected()),this, SLOT(doDisconnect()));

}

/**
  * Smazani vsech alokovanych dat
  */
MainWindow::~MainWindow()
{

    delete p_color;
    delete t_color;
    delete config_list;
    delete current_style;
    delete server_status_label;
    delete user_status_label;
    delete socket;

    delete ui;
}

/**
  * Pri zavirani hlavniho okna krizkem se musi prvne zavrit vsechny taby
  * @param event    odchycena udalost
  */
void MainWindow::closeEvent(QCloseEvent *event)
{
    Q_UNUSED(event)

    doDisconnect();

    // zavreni vsech tabu
    for (int i=ui->tabWidget->count(); i > 0;--i)
    {
        closeTab(ui->tabWidget->currentIndex());
    }

    writeSettings();

}

/**
  * Zachyceni klavesy delete pri mazani prvku
  * @param event    odchycena udalost
  */
void MainWindow::keyPressEvent(QKeyEvent *event)
{
    Q_UNUSED(event)

    if (event->key()==Qt::Key_Delete)
        deleteItem();

}

/**
  * Spojeni se serverem pomoci connectdialogu
  */
void MainWindow::doConnect()
{
    // nastavi se uzivatelem zadane host name
    this->host_name=connect_form->getServer();
    if (this->host_name == "")
    {
        connect_form->setStatus("Problem with address ...");
        return;
    }

    // nastavi se uzivatelem zadany port
    this->port=connect_form->getPort();
    if (this->port == 0)
    {
        connect_form->setStatus("Problem with port ...");
        return;
    }

    QHostAddress addr;

    if (QString::compare(host_name, "localhost", Qt::CaseInsensitive)==0)
    {
        addr.setAddress("127.0.0.1");
    }
    else
    {
        QHostInfo info = QHostInfo::fromName(this->host_name);

        if (info.error() != 0)
        {
            connect_form->setStatus("Problem with hostname ...");
            return;
        }

        addr= info.addresses().first();
    }


    socket->abort();
    socket->connectToHost(addr,port);

    if (socket->waitForConnected(1000))
    {

        connect_form->done(0);
        //return;

    }
    else
    {
        connect_form->setStatus("Unable to connect");
        return;
    }

}

/**
  * Pri signalu disconnected se provedou tyto akce
  */
void MainWindow::doDisconnect()
{
    socket->disconnectFromHost();
    socket->close();
    server_status_label->setText("Disconnected");
    user_status_label->setText("---");

    ui->actionConnect->setDisabled(false);
    ui->actionLogout->setDisabled(true);
    ui->actionOpen_from_server->setDisabled(true);
    ui->actionSave_on_server->setDisabled(true);

    simulationStepButton->setDisabled(true);
    simulationRunButton->setDisabled(true);
    historyButton->setDisabled(true);

}

/**
  * Prihlaseni na server pomoci login dialogu
  */
void MainWindow::doLogin()
{

    this->user_name=login_form->getLogin();
    if (this->user_name == "")
    {
        login_form->setStatus("No name given :(");
        return;
    }
    QString password=login_form->getPassword();
    if (password == "")
    {
        login_form->setStatus("You must enter password !!");
        return;
    }

    QString message="LOGIN\n" + user_name + "\n" + password + "\nEOF";

    socket->write(message.toAscii());
    if (!socket->waitForReadyRead(3000))
    {
        QMessageBox::critical(this, tr("Error"),tr("Connection lost"));
        doDisconnect();
        login_form->close();
        return;
    }

    QByteArray ba;
    ba = socket->readAll();

    if (ba.contains("OK")) // uspesne zalogovani
    {
        user_status_label->setText("Logged as: " + this->user_name);
        server_status_label->setText(this->host_name);

        ui->actionConnect->setDisabled(true);
        ui->actionLogout->setDisabled(false);
        ui->actionOpen_from_server->setDisabled(false);
        ui->actionSave_on_server->setDisabled(false);

        simulationStepButton->setDisabled(false);
        simulationRunButton->setDisabled(false);
        historyButton->setDisabled(false);

        login_form->clearData();
        login_form->close();
        this->setDisabled(false);
    }
    else if (ba.contains("DOESNTEXIST"))
    {
        login_form->setStatus("User doesn't exist");
        return;
    }
    else if (ba.contains("WRONGPASSWORD"))
    {
        login_form->setStatus("Wrong password!");
        return;
    }
    else
    {
        login_form->setStatus("Unknown response from server");
        return;
    }

}

/**
  * Vytvoreni noveho uzivatele v login dialogu
  */
void MainWindow::doNewUser()
{
    this->user_name=login_form->getLogin();
    if (this->user_name == "")
    {
        login_form->setStatus("No name given");
        return;
    }

    QString password=login_form->getPassword();
    if (password == "")
    {
        login_form->setStatus("You must enter password!");
        return;
    }

    QString message="NEWUSER\n" + user_name + "\n" + password + "\nEOF";

    socket->write(message.toAscii());
    if (!socket->waitForReadyRead(3000))
    {
        QMessageBox::critical(this, tr("Error"),tr("Connection lost"));
        doDisconnect();
        login_form->close();
        return;
    }

    QByteArray ba;
    ba = socket->readAll();


    if (ba.contains("OK")) // uspesne vytvoreni noveho uzivatele
    {
        login_form->setStatus("User created");

    }
    else if (ba.contains("ALREADYEXISTS"))
    {
        login_form->setStatus("User already exists");
        return;
    }
    else
    {
        login_form->setStatus("Unknown response from server");
        return;
    }

}

/**
  * Pri kliknuti na tlacitko Connect se zajisti otevreni connect dialogu
  */
void MainWindow::openConnectDialog()
{
    connect_form = new ConnectDialog(this);

    connect(connect_form,SIGNAL(click_connect()),this, SLOT(doConnect()));

    connect_form->show();
    connect_form->activateWindow();

}

/**
  * Pri signalu connected se zajisti otevreni login dialogu
  */
void MainWindow::openLoginDialog()
{
    login_form = new LoginDialog(this);

    connect(login_form,SIGNAL(click_login()),this, SLOT(doLogin()));
    connect(login_form,SIGNAL(click_newuser()),this, SLOT(doNewUser()));
    connect(login_form,SIGNAL(click_storno()),this, SLOT(doDisconnect()));

    login_form->show();
    login_form->activateWindow();

}

/**
  * Otevreni lokalni site pri kliknuti na "open net local"
  */
void MainWindow::openNetLocal()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Open Petri network"), QApplication::applicationDirPath()+"/nets/", tr("Petri net files (*.pn);;All files (*.*)"));
    if (fileName=="") return;

    // vytvori se novy tab s novou scenou
    this->addNewTab(fileName);

    EditorScene * scene = (EditorScene *)((QGraphicsView *)ui->tabWidget->currentWidget())->scene();

    // nacteni vsech elementu do sceny
    XmlParser reader(scene);
    if (reader.readFile(fileName))
    {
        setWindowTitle(qAppName()+" - "+fileName);
        ui->tabWidget->setTabText(ui->tabWidget->currentIndex(),scene->name+" ["+QString::number(scene->version)+"]");
        scene->saved=true;
        scene->fileName = fileName;
        scene->setMaxID();
        changeStyle(*current_style);
    }

}

/**
  * Ulozeni lokalni site pri kliknuti na "save as"
  */
void MainWindow::saveNetAs()
{
    // ziskam scenu z aktualniho tabu
    EditorScene * scene = (EditorScene *)((QGraphicsView *)ui->tabWidget->currentWidget())->scene();

    // save dialog - ziskam z nej cestu kam se ma soubor ulozit
    QFileDialog dialog(this);
    dialog.setDefaultSuffix("pn");
    QString fileName = dialog.getSaveFileName(this,
                                              tr("Save Petri network"),
                                              QApplication::applicationDirPath()+"/nets/",
                                              tr("Petri net files (*.pn);;All files (*.*)"));
    if (fileName=="") return;
    if (fileName.right(3)!=".pn")
        fileName.append(".pn");

    scene->fileName = fileName;
    scene->saved=false;
    scene->version=0;
    saveNet();
}

/**
  * Ulozeni lokalni site pri kliknuti na "save"
  */
void MainWindow::saveNet()
{
    // ziskam scenu z aktualniho tabu
    EditorScene * scene = (EditorScene *)((QGraphicsView *)ui->tabWidget->currentWidget())->scene();

    // kdyz je scena ulozena, neni ji treba ukladat
    if (scene->saved==true)
        return;

    if (scene->fileName=="")
    {
        saveNetAs();
        return;
    }

    scene->version++;

    QString fileName = scene->fileName;

    // otevreni souboru - kdyz se nepovede, tak konec
    QFile file(fileName);
    if (!file.open(QFile::WriteOnly | QFile::Text)) {
        QMessageBox::critical(this, tr("Error: Cannot write file"),
                              fileName + ": " +
                              file.errorString());
        scene->fileName = "";
        return;
    }

    // vytvoreni hlavni xml struktury a korenoveho elementu <petri>
    QXmlStreamWriter xmlWriter(&file);
    xmlWriter.setAutoFormatting(true);
    xmlWriter.writeStartDocument();
    xmlWriter.writeStartElement("petri");

    // element <net> a jeho atributy
    xmlWriter.writeStartElement("net");
    xmlWriter.writeAttribute("name", scene->name);
    xmlWriter.writeAttribute("login", scene->login);
    xmlWriter.writeAttribute("version", QString::number(scene->version));
    xmlWriter.writeAttribute("description", scene->description);

    // ulozeni vsech mist(<places>) - musi byt driv nez prechody (<arc>)
    QHash<QString, ItemNode *>::const_iterator iter_places = scene->scenePlaceList->constBegin();
    while (iter_places != scene->scenePlaceList->constEnd()) {
        xmlWriter.writeStartElement("place");
        QPointF pos = iter_places.value()->scenePos();
        xmlWriter.writeAttribute("x", QString::number(pos.x()));
        xmlWriter.writeAttribute("y", QString::number(pos.y()));
        xmlWriter.writeAttribute("id", iter_places.value()->item_name);

        // prevede QList<Int> na QString oddeleny ", "
        QString tokens = "";
        QList<int>::iterator j = iter_places.value()->tokens->begin();
        while (j != iter_places.value()->tokens->end()) {
            tokens.append(QString::number(*j));
            tokens.append(", ");
            ++j;
        }

        // odreze posledni ", "
        if (tokens != "") tokens.chop(2);

        xmlWriter.writeAttribute("tokens", tokens);
        xmlWriter.writeEndElement();
        ++iter_places;
    }

    // ulozeni vsech prechodu(<transition>) - musi byt driv nez prechody (<arc>)
    QHash<QString, ItemNode *>::const_iterator iter_transitions = scene->sceneTransitionList->constBegin();
    while (iter_transitions != scene->sceneTransitionList->constEnd()) {
        xmlWriter.writeStartElement("transition");
        QPointF pos = iter_transitions.value()->scenePos();
        xmlWriter.writeAttribute("x", QString::number(pos.x()));
        xmlWriter.writeAttribute("y", QString::number(pos.y()));
        xmlWriter.writeAttribute("id", iter_transitions.value()->item_name);

        // prevede QList<QString> na QString oddeleny ", "
        QString guards = "";
        QList<QString>::iterator iter_guard = iter_transitions.value()->guardList->begin();
        while (iter_guard != iter_transitions.value()->guardList->end()) {
            guards.append(*iter_guard);
            guards.append(" & ");
            ++iter_guard;
        }

        // odreze posledni " & "
        if (guards != "") guards.chop(3);

        xmlWriter.writeAttribute("guards", guards);
        xmlWriter.writeAttribute("expression", iter_transitions.value()->str_guardexpr);
        xmlWriter.writeEndElement();
        ++iter_transitions;
    }

    // ulozeni vsech prechodu(<arc>)
    QHash<QString, ItemArc *>::const_iterator iter_arcs = scene->sceneArcList->constBegin();
    while (iter_arcs != scene->sceneArcList->constEnd()) {
        xmlWriter.writeStartElement("arc");
        xmlWriter.writeAttribute("start", iter_arcs.value()->from->item_name);
        xmlWriter.writeAttribute("stop", iter_arcs.value()->to->item_name);
        xmlWriter.writeAttribute("id", iter_arcs.value()->ID);
        xmlWriter.writeAttribute("name", iter_arcs.value()->arc_name);
        xmlWriter.writeEndElement();
        ++iter_arcs;
    }


    // zapsana konce dokumentu, uzaviraci tagy...
    xmlWriter.writeEndDocument();
    file.close();
    if (file.error()) {
        QMessageBox::critical(this, tr("Error: Cannot write file"),
                              fileName + ": " +
                              file.errorString());
        return;
    }

    setWindowTitle(qAppName()+" - "+fileName);
    ui->tabWidget->setTabText(ui->tabWidget->currentIndex(),scene->name+" ["+QString::number(scene->version)+"]");
    scene->saved=true;
}

/**
  * Nacteni site ze serveru pri kliknuti na "open net from server"
  */
void MainWindow::openNetFromServer()
{

    OpenFromServerDialog *opendialog = new OpenFromServerDialog(socket);

    if (!opendialog->showDialog())
    {
        return;
    }

    // zprava ktera si rika serveru o poslani souboru
    // OPEN_NET filename
    QString message = "OPEN_NET\n"+opendialog->filename + "\nEOF";
    socket->write(message.toAscii());

    if (!socket->waitForReadyRead(3000))
    {
        QMessageBox::critical(this, tr("Error"),tr("Connection lost"));
        doDisconnect();
        return;
    }
    QByteArray answer;

    answer = socket->readAll();

    while (!answer.contains("\nEOF"))
    {
        socket->waitForReadyRead(5000);
        answer.append(socket->readAll());
    }

    answer.remove(answer.lastIndexOf("\nEOF"),4);


    // odpoved od serveru
    if (answer=="FAIL")
    {
        QMessageBox::critical(this, tr("Error"),tr("Server can't send requested file"));
        return;
    }

    QString tempName="temp"+QString::number(tempCount++);

    // otevreni souboru pro zapis
    QFile file(tempName);
    if (!file.open(QFile::WriteOnly | QFile::Text)) {
        QMessageBox::critical(this, tr("Error"),
                              tr("can't open temp file for write")+tempName+tr(" : ")+file.errorString());
        return;
    }

    // zapsani docasneho souboru na disk
    file.write(answer.mid(QString("FILE\n").length()));
    file.close();
    if (file.error()) {
        QMessageBox::critical(this, tr("Error"),
                              tr("Error while closing file")+tempName+tr(" : ")+file.errorString());
        return;
    }


    this->addNewTab(tempName);

    EditorScene * scene = (EditorScene *)((QGraphicsView *)ui->tabWidget->currentWidget())->scene();

    // nacteni do sceny z ulozeneho temp souboru
    XmlParser reader(scene);
    if (reader.readFile(tempName))
    {
        setWindowTitle(qAppName()+" - from server");
        ui->tabWidget->setTabText(ui->tabWidget->currentIndex(),scene->name+" ["+QString::number(scene->version)+"]");
        scene->saved=true;
        scene->fileName = "";
        scene->setMaxID();
        changeStyle(*current_style);
    }
    else
    {
        return;
    }

    // smazani tempu
    QFile::remove(tempName);

}

/**
  * Ulozeni site na server pri kliknuti na "save on server"
  */
void MainWindow::saveNetOnServer()
{

    // ziskam scenu aktualniho tabu
    EditorScene * scene = (EditorScene *)((QGraphicsView *)ui->tabWidget->currentWidget())->scene();

    // kdyz jeste soubor nebyl ulozen, tak ho ulozime do docasneho souboru temp.pn
    bool temp=false;
    if ( scene->fileName == "" )
    {
        scene->fileName="temp.pn";
        scene->saved = false;
        temp = true;
        saveNet();
    }

    // pripravna zprava se zakladnimi udaji o posilane siti
    // SAVE_PREPARE login jmeno_site verze
    QString message = "SAVE_PREPARE\n"+scene->login+"\n"+scene->name+"\n"+QString::number(scene->version)+"\n"+scene->description + "\nEOF";
    socket->write(message.toAscii());
    if (!socket->waitForReadyRead(3000))
    {
        QMessageBox::critical(this, tr("Error"),tr("Connection lost"));
        doDisconnect();
        return;
    }

    // odpoved od serveru
    QByteArray answer;
    answer = socket->readAll();

    if (! answer.contains("OK"))
    {
        QList<QByteArray> msg = answer.split('\n');
        if (msg.isEmpty() || answer.contains("FAIL"))
        {
            if (this->sender()==ui->actionSave_on_server)
                QMessageBox::critical(this, tr("FAIL"),
                                      tr("can't save file ")+scene->fileName);
            return;
        }
        else if ( answer.contains("ALREADY_EXISTS"))
        {
            QString vers_str = msg.at(1).data();
            int vers = vers_str.toInt();
            scene->version = vers;
            ui->tabWidget->setTabText(ui->tabWidget->currentIndex(),scene->name+" ["+QString::number(scene->version)+"]");
            if (temp) scene->fileName="";
            else
            {
                scene->version--;
                scene->saved=false;
                saveNet();
            }
            saveNetOnServer();
            return;
        }
        return;
    }


    // otevru si soubor ktery je v aktualnim tabu
    QFile file(scene->fileName);
    if (!file.open(QFile::ReadOnly | QFile::Text)) {
        QMessageBox::critical(this, tr("Error"),
                              tr("Can't open file ")+scene->fileName);
        return;
    }
    // nactu soubor do ByteArray
    QByteArray xmlba;
    xmlba = file.readAll();
    xmlba.push_front("SAVE\n");

    xmlba.push_back("\nEOF");

    // poslani souboru na server
    socket->write(xmlba);
    if (!socket->waitForReadyRead(3000))
    {
        QMessageBox::critical(this, tr("Error"),tr("Connection lost"));
        doDisconnect();
        return;
    }

    // kontrola jak dopadlo ukladani
    answer = socket->readAll();
    if (answer.contains("OK"))
    {
        if (this->sender()==ui->actionSave_on_server)
            QMessageBox::information(this, tr("OK"),
                                     tr("File saved ")+scene->name);
    }

    // zavreme soubor a kdyz to byl jen doceasny, tak ho smazeme
    file.close();
    if (temp)
    {
        scene->fileName="";
        setWindowTitle(qAppName()+" - not saved");
        QFile::remove("temp.pn");

    }

}

/**
  * Zobrazi napovedu
  */
void MainWindow::aboutDialog()
{
    Ui::aboutDialog ui_about;
    QDialog d;
    ui_about.setupUi(&d);
    d.exec();
}

/**
  * Vytvoreni vsech tlacitek na toolbaru pri startu aplikace
  */
void MainWindow::setupToolbar()
{
    editToolbar = addToolBar(tr("Edit"));

    QToolButton *moveButton = new QToolButton(this);
    moveButton->setCheckable(true);
    moveButton->setChecked(true);
    moveButton->setIcon(QIcon(QPixmap(":/images/01.png")));
    moveButton->setToolTip("Move");

    QToolButton *newPlaceButton = new QToolButton(this);
    newPlaceButton->setCheckable(true);
    newPlaceButton->setIcon(QIcon(QPixmap(":/images/02.png")));
    newPlaceButton->setToolTip("Add new place");

    QToolButton *newTransitionButton = new QToolButton(this);
    newTransitionButton->setCheckable(true);
    newTransitionButton->setIcon(QIcon(QPixmap(":/images/03.png")));
    newTransitionButton->setToolTip("Add new transition");

    QToolButton *newArcButton = new QToolButton(this);
    newArcButton->setCheckable(true);
    newArcButton->setIcon(QIcon(QPixmap(":/images/04.png")));
    newArcButton->setToolTip("Add new arc");

    editToolGroup = new QButtonGroup(this);
    editToolGroup->setExclusive(true);

    editToolGroup->addButton(moveButton,0);
    editToolGroup->addButton(newPlaceButton,1);
    editToolGroup->addButton(newTransitionButton,2);
    editToolGroup->addButton(newArcButton,3);

    connect(editToolGroup,SIGNAL(buttonClicked(int)),this,SLOT(toolGroupClicked()));

    editToolbar->addWidget(moveButton);
    editToolbar->addWidget(newPlaceButton);
    editToolbar->addWidget(newTransitionButton);
    editToolbar->addWidget(newArcButton);

    editToolGroup->setExclusive(true);

    editToolbar->addSeparator();

    QToolButton *deleteButton = new QToolButton(this);
    deleteButton->setIcon(QIcon(QPixmap(":/images/06.png")));
    deleteButton->setToolTip("Delete item");

    QToolButton *deleteAllButton = new QToolButton(this);
    deleteAllButton->setIcon(QIcon(QPixmap(":/images/07.png")));
    deleteAllButton->setToolTip("Delete all items");

    QPushButton * propertiesButton = new QPushButton(this);
    propertiesButton->setText("Properties");

    editToolbar->addWidget(deleteButton);
    editToolbar->addWidget(deleteAllButton);
    editToolbar->addWidget(propertiesButton);

    editToolbar->addSeparator();

    QLabel *label=new QLabel("Simulation: ");

    simulationRunButton = new QPushButton(this);
    simulationRunButton->setText("Run");
    simulationRunButton->setDisabled(true);
    simulationRunButton->setMaximumWidth(75);

    simulationStepButton = new QPushButton(this);
    simulationStepButton->setText("Step");
    simulationStepButton->setDisabled(true);
    simulationStepButton->setMaximumWidth(75);

    historyButton = new QPushButton(this);
    historyButton->setText("History");
    historyButton->setDisabled(true);
    historyButton->setMaximumWidth(80);

    editToolbar->addWidget(label);
    editToolbar->addWidget(simulationRunButton);
    editToolbar->addWidget(simulationStepButton);
    editToolbar->addWidget(historyButton);

    editToolbar->addSeparator();

    QLabel *label2=new QLabel("Style: ",this);
    editToolbar->addWidget(label2);

    QComboBox * combobox = new QComboBox(this);
    QStringList style_list,slist;


    for (int i = 0; i < config_list->length(); ++i)
    {
        slist=config_list->at(i);
        style_list<<slist[0];
    }

    combobox->insertItems(0,style_list);
    combobox->setCurrentIndex(*current_style);

    editToolbar->addSeparator();
    editToolbar->addWidget(combobox);


    connect(combobox, SIGNAL(activated(int)),this,SLOT(changeStyle(int)));

    connect(deleteButton,SIGNAL(clicked()),this,SLOT(deleteItem()));
    connect(deleteAllButton,SIGNAL(clicked()),this,SLOT(deleteAll()));
    connect(propertiesButton,SIGNAL(clicked()),this,SLOT(editProperties()));

    connect(historyButton,SIGNAL(clicked()),this,SLOT(showHistory()));
    connect(simulationRunButton,SIGNAL(clicked()),this,SLOT(simulationRun()));
    connect(simulationStepButton,SIGNAL(clicked()),this,SLOT(simulationStep()));

}

/**
  * Otevre dialog, kam uzivatel zada nazev nove site
  */
void MainWindow::addNewTabSlot()
{
    bool ok;
    QString text = QInputDialog::getText(this, tr("New petri net"), tr("Name: "), QLineEdit::Normal, QString(), &ok);
    if (ok && !text.isEmpty())
        addNewTab(text);
}

/**
  * Vytvori novy tab a scenu s nazvem tabtitle
  * @param tabtitle nazev noveho tabu a sceny
  */
void MainWindow::addNewTab(QString tabtitle)
{
    EditorScene * scene = new EditorScene(tabtitle, p_color, t_color, txt_color);

    QGraphicsView *view= new QGraphicsView();
    view->setScene(scene);
    view->setDragMode(QGraphicsView::RubberBandDrag);
    int index = ui->tabWidget->addTab( view, tabtitle);
    ui->tabWidget->setTabsClosable(true);
    ui->tabWidget->setMovable(true);
    ui->tabWidget->setCurrentIndex(index);
}

/**
  * Pri zmene aktivniho tabu se aktualizuji nazvy
  */
void MainWindow::tabSwitched()
{
    EditorScene * scene = (EditorScene *)((QGraphicsView *)ui->tabWidget->currentWidget())->scene();
    if (scene->fileName != "")
        setWindowTitle(qAppName()+" - "+scene->fileName);
    else
        setWindowTitle(qAppName()+" - not saved");

    changeStyle(*current_style);
}

/**
  * Pri kliknuti na tlacitko "delete all" se smazou vsechny objekty na scene
  */
void MainWindow::deleteAll()
{
    EditorScene * scene = (EditorScene *)((QGraphicsView *)ui->tabWidget->currentWidget())->scene();

    scene->deleteAll();

}

/**
  * Pri kliknuti na tlacitko "delete item" se smazou vsechny vybrane objekty na scene
  */
void MainWindow::deleteItem()
{
    EditorScene * scene = (EditorScene *)((QGraphicsView *)ui->tabWidget->currentWidget())->scene();

    scene->deleteItem();

}

/**
  * Pri kliknuti v menu na "close" se emituje signal na zavreni aktualniho tabu
  */
void MainWindow::closeCurrentTab()
{
    emit closeTab(ui->tabWidget->currentIndex());
}

/**
  * Zavre tab na pozici index
  * @param index    tab, ktery se ma zavrit
  */
void MainWindow::closeTab(int index)
{
    EditorScene * scene = (EditorScene *)(((QGraphicsView *)ui->tabWidget->widget(index))->scene());

    int ret=QMessageBox::Cancel;


    if (!scene->saved)
    {
        QMessageBox msgBox;
        msgBox.setText(scene->name+" has been modified.");
        msgBox.setInformativeText("Do you want to save changes?");
        msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Save);
        int ret = msgBox.exec();


        switch (ret) {
        case QMessageBox::Save:
            saveNet();
            break;
        case QMessageBox::Discard:
            scene->saved=true;
            break;
        case QMessageBox::Cancel:
            return;
            break;
        default:
            break;
        }
    }

    if (scene->saved || ret==QMessageBox::Discard)
    {
        scene->saved=true;

        scene->deleteAll();


        if (ui->tabWidget->count() > 1)
        {
            ui->tabWidget->removeTab(index);
        }
        else
        {
            ui->tabWidget->setTabText(0,"new_petri_net");
            setWindowTitle(qAppName()+ " - not saved");
            scene->name = "new_petri_net";
            scene->version = 1;
            scene->fileName = "";
            scene->login = "";
            scene->description = "";
            scene->saved = true;
        }

    }

}

/**
  * Pri kliknuti na "Properties" se otevre dialog na upravu vlastnosti vybraneho objektu.
  * Pokud neni vybran zadny objekt, tak se edituji vlastnosti site, kdyz jich je vybrano vice, nic se neprovede
  */
void MainWindow::editProperties()
{
    EditorScene * scene = (EditorScene *)((QGraphicsView *)ui->tabWidget->currentWidget())->scene();
    QList<QGraphicsItem *> selecteditems = scene->selectedItems();

    QDialog *dialog= new QDialog();
    dialog->setWindowFlags(Qt::Widget);
    QVBoxLayout *vbox = new QVBoxLayout(dialog);

    // nic neni vybrano, edituje se sit samotna
    if (selecteditems.count() == 0)
    {
        dialog->setWindowTitle("Properties of network");

        QLabel * label = new QLabel("Name",dialog);
        QLineEdit * lineedit = new QLineEdit(scene->name,dialog);

        QLabel * label2 = new QLabel("Author: "+ scene->login,dialog);
        QLineEdit * lineedit2 = new QLineEdit(scene->login,dialog);

        QLabel * label3 = new QLabel("Version: "+ QString::number(scene->version),dialog);
        QLabel * label4 = new QLabel("File name: "+ scene->fileName,dialog);

        QLabel * label5 = new QLabel("Description: ",dialog);

        QPlainTextEdit * textedit = new QPlainTextEdit(scene->description,dialog);

        QDialogButtonBox *bbox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,Qt::Horizontal,dialog);

        vbox->addWidget(label);
        vbox->addWidget(lineedit);
        vbox->addWidget(label2);
        vbox->addWidget(lineedit2);
        vbox->addWidget(label3);
        vbox->addWidget(label4);
        vbox->addWidget(label5);
        vbox->addWidget(textedit);
        vbox->addWidget(bbox);

        connect(bbox, SIGNAL(accepted()), dialog, SLOT(accept()));
        connect(bbox, SIGNAL(rejected()), dialog, SLOT(reject()));

        dialog->setLayout(vbox);
        int res=dialog->exec();
        if (res==QDialog::Accepted)
        {
            // pokud uzivatel zmenil hodnotu name
            if (scene->name != lineedit->text())
            {
                scene->name = lineedit->text();
                ui->tabWidget->setTabText(ui->tabWidget->currentIndex(),scene->name+" ["+QString::number(scene->version)+"]");
                scene->saved = false;
            }

            // pokud uzivatel zmenil hodnotu description
            if (scene->description != textedit->toPlainText())
            {
                scene->description = textedit->toPlainText();
                scene->saved = false;
            }

            // pokud uzivatel zmenil hodnotu login
            if (scene->login != lineedit2->text())
            {
                scene->login=lineedit2->text();
                scene->saved = false;
            }

        }

    }
    // vybran prave jeden objekt
    else if (selecteditems.count() == 1)
    {
        scene->editProperties();
    }

}

/**
  * Provede pozadek na server o zobrazeni simulaci prave otevrene site. Ty nasledne zobrazi v novem okne
  */
void MainWindow::showHistory()
{
    EditorScene * scene = (EditorScene *)((QGraphicsView *)ui->tabWidget->currentWidget())->scene();

    // priprava pozadavku na server a jeho poslani
    QString message = "LIST_RUNS\n"+scene->name + "\nEOF";
    socket->write(message.toAscii());

    if (!socket->waitForReadyRead(3000))
    {
        QMessageBox::critical(this, tr("Error"),tr("Connection lost"));
        doDisconnect();
        return;
    }

    // odpoved od serveru
    QByteArray ba;
    ba = socket->readAll();

    QList<QByteArray> msg = ba.split('\n');
    if (msg.first()=="RUNS")
    {
        QString str=ba.data();
        QStringList slist=str.split("\n",QString::SkipEmptyParts);
        slist.removeFirst();

        // novy dialog
        QDialog *dialog= new QDialog();
        dialog->setWindowFlags(Qt::Widget);
        dialog->setMinimumWidth(400);

        QVBoxLayout *vbox = new QVBoxLayout(dialog);

        QLabel * label = new QLabel("Run history of net "+scene->name);
        QListWidget *mylist= new QListWidget(dialog);

        QDialogButtonBox *bbox = new QDialogButtonBox(QDialogButtonBox::Ok,Qt::Horizontal,dialog);

        vbox->addWidget(label);
        vbox->addWidget(mylist);
        vbox->addWidget(bbox);


        if (slist.empty())
        {
            QListWidgetItem *item = new QListWidgetItem;
            item->setText("No records found");
            mylist->insertItem(0,item);
        }
        else
        {
            // pridani vsech zaznamu o simulaci do okna se simulacemi
            foreach(QString s, slist)
            {
                QListWidgetItem *item = new QListWidgetItem;
                item->setText(s);
                mylist->insertItem(0,item);
                mylist->setCurrentRow(0);
            }
        }

        connect(bbox, SIGNAL(accepted()), dialog, SLOT(accept()));
        dialog->exec();

    }

}

/**
  * Nastavi aktualne vybranou akci (move, insert place, insert transition, insert arc)
  */
void MainWindow::toolGroupClicked()
{

    EditorScene * scene = (EditorScene *)((QGraphicsView *)ui->tabWidget->currentWidget())->scene();

    if (scene == 0)
        return;

    scene->setAction(EditorScene::Actions(editToolGroup->checkedId()));
}

/**
  * Provede kompletni simulaci, sit otevrena ve scene se ulozi a posle se na server. Ten celou sit
  * odsimuluje a vrati klientu aktualizovany seznam tokenu v siti.
  */
void MainWindow::simulationRun()
{
    EditorScene * scene = (EditorScene *)((QGraphicsView *)ui->tabWidget->currentWidget())->scene();

    // pokud nejsou na scene prechody, tak neni potreba simulovat
    if (scene->sceneArcList->empty())
        return;

    saveNetOnServer();

    // priprava pozadavku na simulaci serveru a jeho odeslani
    QString message = "RUN_SIMULATION\n"+scene->name+"\n"+QString::number(scene->version) + "\nEOF";
    socket->write(message.toAscii());
    if (!socket->waitForReadyRead(3000))
    {
        QMessageBox::critical(this, tr("Error"),tr("Connection lost"));
        doDisconnect();
        return;
    }

    // odpoved od serveru
    QByteArray ba;
    ba = socket->readAll();


    QList<QByteArray> msg = ba.split('\n');
    // kdyz jsou v siti nevazane promenne, tak se nemuze simulovat
    if (msg.first()=="UNBOUNDVARIABLE")
    {
        QMessageBox::warning(this, tr("Can't simulate petri net"), tr("Unbound variable ")+msg[1].data());
        return;
    }
    else if (msg.first()=="RUN_OK")
    {
        // aktualizujeme tokeny v mistech site

        QString s= ba.data();
        QStringList sl=s.split("\n");
        sl.takeFirst();

        scene->actualizeTokens(sl);

        QMessageBox::information(this,tr("Congratulations"),tr("Simulation finished"));
        return;

    }

}


/**
  * Provede jeden krok simulace, sit otevrena ve scene se ulozi, posle na server, ten vypocte proveditelne
  * prechody a posle je k vyberu. Klient si vybere v dialogu kterych prechod a sadu tokenu chce pouzit a posle
  * tuto informaci serveru. Ten nasledne vrati klientu aktualizovany seznam tokenu site.
  */
void MainWindow::simulationStep()
{
    EditorScene * scene = (EditorScene *)((QGraphicsView *)ui->tabWidget->currentWidget())->scene();

    // kdyz nejsou na scene zadne prechody nemusi se ani simulovat
    if (scene->sceneArcList->empty())
        return;

    // simulovana sit musi byt ulozena na server
    saveNetOnServer();

    // stvoreni pozadavku pro server a poslani
    QString message = "RUN_STEP\n"+scene->name+"\n"+QString::number(scene->version) + "\nEOF";
    socket->write(message.toAscii());
    if (!socket->waitForReadyRead(3000))
    {
        QMessageBox::critical(this, tr("Error"),tr("Connection lost"));
        doDisconnect();
        return;
    }

    // odpoved od serveru
    QByteArray ba;
    ba = socket->readAll();

    QList<QByteArray> msg = ba.split('\n');
    if (msg.first()=="UNBOUNDVARIABLE")
    {
        QMessageBox::warning(this, tr("Can't simulate petri net"), tr("Unbound variable ")+msg[1].data());
        return;
    }
    else if (msg.first()=="CHOOSETRANS") // musime zvolit ktery prechod chceme odpalit
    {
        // dialog ktery vyskoci a uzivatel si zvoli co bude chtit odpalit

        QDialog *dialog= new QDialog();
        dialog->setWindowFlags(Qt::Widget);

        QVBoxLayout *vbox = new QVBoxLayout(dialog);

        QLabel * label = new QLabel("Choose transition to fire",dialog);
        bbox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,Qt::Horizontal,dialog);


        QTreeWidget *treeWidget = new QTreeWidget();
        treeWidget->setColumnCount(1);

        QList<QTreeWidgetItem *> items;

        // prochazeni vsech moznych prechodu
        for (int i = 1; i < msg.count(); ++i)
        {
            QByteArray onerow=msg.at(i);
            QList<QByteArray> splitrow = onerow.split(';');

            QTreeWidgetItem *Item = new QTreeWidgetItem(treeWidget);
            Item->setText(0, splitrow.at(0));

            // prochazeni vsech moznych sad tokenu
            for (int j = 1; j < splitrow.count(); ++j)
            {
                QTreeWidgetItem *subItem = new QTreeWidgetItem(Item);
                subItem->setText(0, splitrow.at(j));
            }

            items.append(Item);
        }
        treeWidget->insertTopLevelItems(0, items);

        vbox->addWidget(label);
        vbox->addWidget(treeWidget);
        vbox->addWidget(bbox);

        dialog->setLayout(vbox);

        bbox->buttons().first()->setDisabled(true);
        connect(treeWidget, SIGNAL(itemClicked(QTreeWidgetItem*,int)),this,SLOT(stepDialogChange(QTreeWidgetItem*,int)));
        connect(bbox, SIGNAL(accepted()), dialog, SLOT(accept()));
        connect(bbox, SIGNAL(rejected()), dialog, SLOT(reject()));

        int res=dialog->exec();

        // kdyz se v dialogu klikne na OK
        if (res==QDialog::Accepted)
        {
            QTreeWidgetItem *selectedItem=treeWidget->currentItem();

            // stvoreni pozadavku serveru s vybranym prechodem a sadou
            message = "RUN_STEP_P2\n"+selectedItem->parent()->text(0)+";"+selectedItem->data(0,0).toString() + "\nEOF";

            // zapsani pozadavku
            socket->write(message.toAscii());
            if (!socket->waitForReadyRead(3000))
            {
                QMessageBox::critical(this, tr("Error"),tr("Connection lost"));
                doDisconnect();
                return;
            }

            // odpoved od serveru s aktualizovanymi tokeny
            ba.clear();
            ba = socket->readAll();

            QString s= ba.data();
            QStringList sl=s.split("\n");

            if (sl.takeFirst()=="STEP_OK")
            {
                scene->actualizeTokens(sl);
            }

        }

    }
    else if (msg.first()=="NOTRANS")
    {
          QMessageBox::information(this,tr("Information"),tr("Nothing to simulate"));

    }

}

/**
  * Pomocny slot pro dialog pri krokovani simulace, kde klientovy prijde na vyber seznam proveditelnych prechodu
  * pokud neni vybrana konkretni kombinace, tak se deaktivuje tlacitko OK
  */
void MainWindow::stepDialogChange(QTreeWidgetItem* item,int)
{
    if (item->text(0).at(0)=='T')
        bbox->buttons().first()->setDisabled(true);
    else
        bbox->buttons().first()->setDisabled(false);

}

/**
  * Zmeni aktualni styl na jiny uzivatelem vybrany
  * @param style    index pozadovaneho styl
  */
void MainWindow::changeStyle(int style)
{

    EditorScene * scene = (EditorScene *)((QGraphicsView *)ui->tabWidget->currentWidget())->scene();

    QStringList slist;

    // nacteni dat z noveho stylu
    *current_style=style;
    slist = config_list->at(style);
    p_color->setNamedColor(slist[1]);
    t_color->setNamedColor(slist[2]);
    txt_color->setNamedColor(slist[3]);

    if (slist[4]=="grid")
        scene->setBackgroundBrush(QBrush(Qt::lightGray, Qt::CrossPattern));
    else
        scene->setBackgroundBrush(QBrush(QColor(slist[4])));

    scene->update();

    // ulozeni do config.xml
    // otevreni souboru - kdyz se nepovede, tak konec
    QFile file("config.xml");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return;
    QTextStream textStream( &file);
    QString fileContent = textStream.readAll();
    file.close();
    fileContent.replace(QRegExp("(<config\\s+default=\")\\d+(\"\\s*>)"), "\\1"+QString::number(style)+"\\2");

    if (!file.open(QFile::WriteOnly | QFile::Text)) {
        QMessageBox::critical(this, tr("Error: Cannot write file config.xml"), file.errorString());
        return;
    }
    QTextStream outstream(&file);
    outstream << fileContent;
    file.close();

}

/**
  * Zapise aktualni nastaveni pozice a velikosti hlavniho okna
  */
void MainWindow::writeSettings()
 {
     QSettings settings("ICP", "pn2012");

     settings.beginGroup("GUI");
     settings.setValue("size", size());
     settings.setValue("pos", pos());
     settings.endGroup();
 }

/**
  * Nacte nastaveni pozice a velikosti hlavniho okna, v pripade ze nastaveni chybi, tak se vezmou defaultni hodnoty
  */
 void MainWindow::readSettings()
 {
     QSettings settings("ICP", "pn2012");

     settings.beginGroup("GUI");
     resize(settings.value("size", QSize(800, 600)).toSize());
     move(settings.value("pos", QPoint(150, 150)).toPoint());
     settings.endGroup();
 }
