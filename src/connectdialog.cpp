/**
 * @file connectdialog.cpp
 * @author Martin Latta
 * @author Michal Hradecky
 */

#include "logindialog.h"
#include "connectdialog.h"
#include "ui_connectdialog.h"

/**
  * Inicializuje pripojovaci dialog
  */
ConnectDialog::ConnectDialog(QWidget *parent) :
    QDialog(parent),
    ui_connect(new Ui::ConnectDialog)
{
    ui_connect->setupUi(this);

    ui_connect->server->setFocus();

}

ConnectDialog::~ConnectDialog()
{
    delete ui_connect;
}

/**
  * Nastavi do textoveho pole pozadovany status
  * @param status   novy status
  */
void ConnectDialog::setStatus(QString status)
{
    ui_connect->label_status->setText(status);
}

/**
  * Vraci text ktery uzivatel zadal do textoveho pole "server"
  * @return uzivatelem zadany nazev server
  */
QString ConnectDialog::getServer()
{
    return ui_connect->server->text();
}

/**
  * Vraci cislo ktere uzivatel zadal do textoveho pole "port"
  * @return uzivatelem zadany port
  */
int ConnectDialog::getPort()
{
    bool pom_bool;
    int p=ui_connect->port->text().toInt(&pom_bool,10);
    if (!pom_bool)
    {
        return 0;
    }
    return p;
}

/**
  * Emituje signal kdyz uzivatel klikne na tlacitko "OK"
  */
void ConnectDialog::on_buttonBox_accepted()
{
    emit click_connect();
}

/**
  * Emituje signal kdyz uzivatel klikne na tlacitko "Storno"
  */
void ConnectDialog::on_buttonBox_rejected()
{
    ConnectDialog::done(0);
}

