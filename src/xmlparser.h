/**
 * @file xmlparser.h
 * @author Martin Latta
 * @author Michal Hradecky
 */

#ifndef XMLPARSER_H
#define XMLPARSER_H

#include <qxml.h>
#include <QXmlStreamReader>
#include <QTreeWidget>
#include <QObject>
#include "editorscene.h"
#include <QtGui>
#include <QtCore>

/**
  * Trida pro parsovani vsech typu XML souboru (sit, uzivatele, config)
  * Trida Xml parser obsahuje metody ke zpracovani vsech pouzitych typu Xml souboru
  * Algoritmus zpracovani Xml dokumentu je inspirovan kapitolou 16 z knihy
  * C++ GUI Programming with Qt4, 2nd Edition by Jasmin Blanchette, Mark Summerfield
  * (www.informit.com/articles/article.aspx?p=1405553)
  * @author Martin Latta
  * @author Michal Hradecky
  */
class XmlParser : public QWidget
{
    Q_OBJECT

public:
    explicit XmlParser(EditorScene * editorScene);
    XmlParser(QList<QStringList> *conf_list,int * curr_style);
    bool readFile(const QString &fileName);

private:
    void readPetriElement();

    void readNetElement();
    void readPlaceElement();
    void readTransitionElement();
    void readArcElement();
    void readRunElement();

    void readUsersElement();
    void readConfigElement();
    void readStyleElement(QString default_id);

    void skipUnknownElement();

    QList<QStringList> *config_list; /** ukazatel na seznam s konfiguracemi */
    int * current_style; /** ukazatel na aktualne pouzity styl */
    EditorScene * scene; /** ukazatel na aktualni scenu */
    QXmlStreamReader reader; /** Xml ctecka */
};

#endif // XMLPARSER_H
