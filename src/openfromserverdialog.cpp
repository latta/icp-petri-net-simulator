/**
 * @file openfromserverdialog.cpp
 * @author Martin Latta
 * @author Michal Hradecky
 */

#include "openfromserverdialog.h"

OpenFromServerDialog::OpenFromServerDialog(QTcpSocket * psocket)
{
    socket=psocket;
}

/**
  * Zobrazi dialog pro nacitani z serveru
  */
bool OpenFromServerDialog::showDialog()
 {

    dialog= new QDialog();
    dialog->setWindowFlags(Qt::Widget);
    dialog->setWindowTitle("Open petri net from server");
    vbox = new QVBoxLayout();

    label_filer= new QLabel("Filter: ");
    hbox=new QHBoxLayout;

    label_names = new QLabel("Nets on server:");
    label_author = new QLabel("Author:");
    lineedit = new QLineEdit();
    nameList= new QListWidget();

    combo_vers = new QComboBox();
    label_vers = new QLabel("Versions:");

    label_desc = new QLabel("Description:");
    edit_desc = new QTextEdit();

    // spojeni se serverem a vypsani jmen vsech siti
    // LIST_NAMES
    QString message = "LIST_NAMES\nEOF";
    socket->write(message.toAscii());
    if (!socket->waitForReadyRead(3000))
    {
        QMessageBox msgbox;
        msgbox.setText("No nets on server");
        msgbox.exec();
        return false;
    }

    // odpoved od serveru
    QString answer;
    answer = socket->readAll();


    // naplneni listu jmeny siti
    nameList->insertItems(0,answer.split("\n"));
    nameList->sortItems(Qt::AscendingOrder);
    nameList->setCurrentRow(0);

    bbox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,Qt::Horizontal,dialog);

    vbox->addWidget(label_names);
    vbox->addLayout(hbox);
    vbox->addWidget(nameList);
    vbox->addWidget(label_vers);
    vbox->addWidget(combo_vers);
    vbox->addWidget(label_author);
    vbox->addWidget(label_desc);
    vbox->addWidget(edit_desc);
    vbox->addWidget(bbox);

    hbox->addWidget(label_filer);
    hbox->addWidget(lineedit);

    QObject::connect(bbox, SIGNAL(accepted()), dialog, SLOT(accept()));
    QObject::connect(bbox, SIGNAL(rejected()), dialog, SLOT(reject()));

    if (nameList->count()!=0)
    {
        getVersions(nameList->item(0));
        getDetails(0);
    }

    QObject::connect(nameList, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(getVersions(QListWidgetItem*)));
    QObject::connect(combo_vers, SIGNAL(currentIndexChanged(int)), this, SLOT(getDetails(int)));

    QObject::connect(lineedit,SIGNAL(textChanged(QString)),this,SLOT(filterNets(QString)));

    dialog->setLayout(vbox);

    int res=dialog->exec();

    if (res==QDialog::Accepted)
    {
        return true;
    }
    return false;
}

/**
  * Ziskani vsech verzi vybrane site ze serveru
  * @param item aktualne vybrana sit
  */
void OpenFromServerDialog::getVersions(QListWidgetItem* item)
{
    combo_vers->clear();
    last_selected = item;
    // spojeni se serverem a vypsani vsech verzi dane site do comboboxu
    // LIST_VERSIONS
    QString message = "LIST_VERSIONS\n"+item->text() + "\nEOF";
    socket->write(message.toAscii());
    if (!socket->waitForReadyRead(3000))
    {
        return;
    }

    // odpoved od serveru
    QByteArray answer;
    answer = socket->readAll();


    QList<QByteArray> vers = answer.split('\n');
    if (vers.isEmpty())
        return;

    QList<QByteArray>::iterator i=vers.begin();
    for (;i<vers.end();++i)
    {
        combo_vers->insertItem(0,(*i));
    }

    combo_vers->setCurrentIndex(0);
}

/**
  * Ziskani informaci o aktualne vybrane verzi site ze serveru
  */
void OpenFromServerDialog::getDetails(int index)
{
    if (index == -1 ) return;

    // spojeni se serverem a vypsani detailu do textboxu
    // LIST_DEsetHiddenTAILS
    QString message = "LIST_DETAILS\n"+nameList->currentItem()->text()+"\n"+combo_vers->currentText() + "\nEOF";
    socket->write(message.toAscii());
    if (!socket->waitForReadyRead(3000))
    {
        return;
    }

    // odpoved od serveru
    QString answer;
    answer = socket->readAll();

    QStringList details = answer.split("\n");
    edit_desc->setText(details[2]);

    label_author->setText("Author: "+details[1]);

    filename=details[0];

}

/**
  * Vypis siti je filtrovan kdyz uzivatel pise do textoveho pole
  * @param filterstr    uzivatelem zadany retezec kterym se filtruji nazvy siti
  */
void OpenFromServerDialog::filterNets(QString filterstr)
{
    int i;
    for (i=0; i < nameList->count();++i)
    {
        if (filterstr.length()!=0)
        {
            // pokud nazev site neobsahuje filterstr jako podretezec, tak se skryje
            if ((nameList->item(i)->text()).indexOf(filterstr)==-1)
                nameList->item(i)->setHidden(true);
            else
                nameList->item(i)->setHidden(false);
        }
        else
            nameList->item(i)->setHidden(false);
    }

    // prvni neskryta sit se nastavi jako aktualni
    for ( i=0; i < nameList->count();++i)
    {
        if (!nameList->item(i)->isHidden())
        {
            nameList->setCurrentRow(i);
            break;
        }
    }

    // pokud jsou vsechny site skryte, nelze kliknout na OK
    if (i!=nameList->count())
    {
        getVersions(nameList->item(i));
        getDetails(0);
        bbox->buttons().first()->setDisabled(false);
    }
    else
    {
        combo_vers->clear();
        bbox->buttons().first()->setDisabled(true);

    }

}
