/**
 * @file openfromserverdialog.h
 * @author Martin Latta
 * @author Michal Hradecky
 */

#ifndef OPENFROMSERVERDIALOG_H
#define OPENFROMSERVERDIALOG_H

#include <QtGui>
#include <QtCore>
#include <QTcpSocket>
#include <QObject>


/**
  * Trida reprezentujici dialog pro otevirani siti ze serveru
  * @author Martin Latta
  * @author Michal Hradecky
  */
class OpenFromServerDialog : public QObject
{
    Q_OBJECT
public:
    OpenFromServerDialog(QTcpSocket * psocket);

    QString filename;   /**< nazev vybrane site */

    bool showDialog();

private slots:
    void getVersions(QListWidgetItem*);
    void getDetails(int index);
    void filterNets(QString filterstr);

private:
    QDialog *dialog;
    QVBoxLayout *vbox;
    QHBoxLayout *hbox;
    QLabel * label_names;
    QLabel * label_filer;
    QLineEdit * lineedit;
    QListWidget *nameList;
    QDialogButtonBox *bbox;
    QLabel * label_vers;
    QComboBox * combo_vers;
    QLabel * label_desc;
    QLabel *label_author;
    QTextEdit *edit_desc;
    QListWidgetItem *last_selected;

    QTcpSocket * socket;

};

#endif // OPENFROMSERVERDIALOG_H
