/**
 * @file itemarc.cpp
 * @author Martin Latta
 * @author Michal Hradecky
 */

#include "itemarc.h"
#include "math.h"
#include <QtGui>
#include <QtCore>

/**
 * Nastaveni vychozich hodnot
 * @param f objekt ze ktereho hrana vychazi
 * @param t objekt do ktereho hrana vstupuje
 * @param color barva textu u hrany
 * @param s scena na kterou se graficky objekt vaze
 *
 */
ItemArc::ItemArc(ItemNode *f, ItemNode *t, QColor * color, EditorScene * s)
{
    QTime time = QTime::currentTime();
    qsrand((uint)time.msec());

    // vygenerovani nahodneho cisla pro posunuti pocatecniho a koncoveho bodu hrany
    int high=3,low=-3;
    rand= qrand() % ((high + 1) - low) + low;
    rand = (rand > 0) ? rand+4 : rand-4;

    scene=s;
    from=f;
    to=t;
    from->addArc(this);
    to->addArc(this);
    Pos_from=from->scenePos();
    Pos_to=to->scenePos();
    arcColor=Qt::black;

    text_color= color;
    setFlag(QGraphicsItem::ItemIsSelectable, true);
}

/**
 * V destruktoru je potreba smazat odkazy u mist a prechodu na ktere je hrana napojena
 */
ItemArc::~ItemArc()
{
    if (from != NULL)
        from->removeArc(this);

    if (to != NULL)
        to->removeArc(this);

    scene->sceneArcList->remove(ID);
}

/**
  * Reimplementovana metoda na nastaveni hranic grafickeho objektu
  * @return objekt re
  */
QRectF ItemArc::boundingRect() const
{
    return QRectF(line().p1(),line().p2()).normalized().adjusted(-5,-5,5,5);
}


/**
  * Nastavi nove jmeno a ID podle parametru
  */
void ItemArc::changeID(QString name)
{
    arc_name=name;
    id_item->setText(name);
}

/**
 * Reimplementovana metoda na vykresleni grafickeho objektu
 */
void ItemArc::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option)
    Q_UNUSED(widget)

    if (from != NULL)
        Pos_from=from->scenePos();
    else
        return;

    if (to != NULL)
        Pos_to=to->scenePos();
    else
        return;


    QLineF cline(Pos_from.x()+rand,Pos_from.y()+rand,Pos_to.x()+rand,Pos_to.y()+rand);
    QPolygonF poly;
    if (to != NULL)
        poly=mapFromItem(to,to->boundingRect());
    else
        return;
    QPointF intersect,p1,p2;
    p1=poly.first();
    for (QPolygonF::iterator i = poly.begin()+1; i!=poly.end(); i++)
    {
        p2=*i;
        if (cline.intersect(QLineF(p1,p2),&intersect) == QLineF::BoundedIntersection)
            break;
        p1=p2;
    }


    setLine(QLineF(intersect,Pos_from));

    qreal angle= acos(line().dx() / line().length());
    if (line().dy() >= 0)
        angle = 6.28 - angle;

    p1= intersect + QPointF(sin(angle + 1.04)*10, cos(angle + 1.04)* 10);
    p2= intersect + QPointF(sin(angle + 2.09)*10, cos(angle + 2.09)* 10);

    poly.clear();
    poly.append(p1);
    poly.append(p2);
    poly.append(intersect);


    painter->setBrush(QBrush(Qt::black));
    painter->drawPolygon(poly);

    painter->setPen(QPen(arcColor,2));
    painter->drawLine(line());

    QLineF myline = line();

    painter->drawLine(myline);

    if (this->isSelected())
    {
        painter->setBrush(Qt::NoBrush);
        painter->setPen(QPen(Qt::gray, 1, Qt::DotLine));
        QLineF outline= line();
        outline.translate(0,4.0);
        painter->drawLine(outline);
        outline.translate(0,-8.0);
        painter->drawLine(outline);
    }

    if (id_item != NULL)
        id_item->setPos(((Pos_from.x()+intersect.x())/2)-((Pos_from.x()-intersect.x())/4),
                        ((Pos_from.y()+intersect.y())/2)-((Pos_from.y()-intersect.y())/4));


}
