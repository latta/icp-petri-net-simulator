/**
 * @file itemnode.h
 * @author Martin Latta
 * @author Michal Hradecky
 */

#ifndef ITEMNODE_H
#define ITEMNODE_H

#include "itemarc.h"
#include "editorscene.h"

#include <QtCore>
#include <QtGui>

class ItemArc;
class EditorScene;

#define TYPPLACE 0
#define TYPTRANS 1

/**
  * Trida reprezentujici graficky objekt uzel (bud misto nebo prechod)
  * @author Martin Latta
  * @author Michal Hradecky
  */
class ItemNode : public QGraphicsItem
{
public:
    ItemNode(int typ, int id, QColor *color, EditorScene *s);
    ~ItemNode();

    enum { Type = UserType + 1};

    enum rel_ops { less,lessequal, greaterequal, greater, equal, notequal };

    int itemtype;       /**< typ uzlu */
    qreal t_size;       /**< sirka prechodu */
    qreal t_size_min;   /**< minimalni sirka prechodu */

    EditorScene *scene; /**< scena na kterou se item vaze */

    QString item_name;      /**< T nebo P + ciselny identifikator */
    int ID;                 /**< ciselny identifikator */

    QGraphicsSimpleTextItem *text_item;         /**< text s nazvem prechodu */

    QGraphicsSimpleTextItem *token_text_item;   /**< text se seznamem tokenu v prechodu */

    QGraphicsSimpleTextItem *guard;             /**< straz v prechodu */
    QGraphicsSimpleTextItem *guardexpr;         /**< operace v prechodu */

    QString str_tokens;     /**< retezec s tokeny */
    QString str_guard;      /**< retezec se strazi */
    QString str_guardexpr;  /**< retezec s podminkou */

    QList<int> * tokens;    /**< seznam tokenu */

    QList<QString> * guardList;     /**< seznam strazi */


    QList<ItemArc *> listOfInputArcs;   /**< seznam vstupnich hran */
    QList<ItemArc *> listOfOutputArcs;  /**< seznam vystupnich hran */

    int type() const { return Type; }

    void removeArc(ItemArc *arc);
    void addArc(ItemArc *arc);

    QRectF boundingRect() const;

    void updateTokenString();
    void updateGuardString();
    void updateTransitionSize();

protected:
    void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* parent);
    QVariant itemChange(GraphicsItemChange change, const QVariant &value);

private:
    QColor *fillColor;      /**< barva vyplne */

};

#endif // ITEMNODE_H
