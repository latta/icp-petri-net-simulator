/**
 * @file logindialog.h
 * @author Martin Latta
 * @author Michal Hradecky
 */

#ifndef LOGINDIALOG_H
#define LOGINDIALOG_H

#include <QDialog>

namespace Ui {
class LoginDialog;
}

/**
  * Trida reprezentujici dialog pro prihlasovani uzivatele
  * @author Martin Latta
  * @author Michal Hradecky
  */
class LoginDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit LoginDialog(QWidget *parent = 0);
    ~LoginDialog();
    
    void setStatus(QString status);
    void clearData();

    QString getLogin();
    QString getPassword();

signals:
    void click_login();
    void click_newuser();
    void click_storno();

private slots:
    void on_Btn_storno_clicked();

    void on_Btn_ok_clicked();

    void on_Btn_new_user_clicked();

private:
    Ui::LoginDialog *ui_login;

};

#endif // LOGINDIALOG_H
