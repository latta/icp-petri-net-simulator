ICP projekt 2012
Simulator petriho siti
-------------

Autori:
- Martin Latta 
- Michal Hradecky

Preklad:
- make v aktualni slozce

Spusteni klienta:
- make run 

Spusteni serveru:
- make runserver

Dokumentace:
- vygeneruje se prikazem make doxygen

Napoveda:
- pri spusteni klienta a kliknuti na Help->About
