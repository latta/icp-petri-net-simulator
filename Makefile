# ICP 2012 projekt - petriho site
# xlatta00 xhrade08
#


.PHONY= all doxygen clean pack run runserver client server

all: client server

client:
	#/usr/local/share/Qt-4.7.3/bin/qmake -o src/Makefile src/pn2012.pro
	qmake -o src/Makefile src/pn2012.pro
	make -C src
	
server:	
	#/usr/local/share/Qt-4.7.3/bin/qmake -o src/server/Makefile src/server/server.pro
	qmake -o src/server/Makefile src/server/server.pro
	make -C src/server
	
	
run: src/pn2012
	cd src;	./pn2012
runserver: src/server/server2012
	cd src/server; ./server2012 10000
pack: clean
	tar czf ../xhrade08.tar.gz ../xhrade08
doxygen:
	doxygen Doxyfile
clean:
	rm -rf doc/* src/*.o src/server/*.o src/pn2012 src/server/server2012 src/Makefile src/server/Makefile src/moc_*.c* src/server/moc_*.c* src/ui*.h src/qrc_resourceimages.cpp
    
